using System;

namespace LMS.Foundation.Amortization
{
    public class InternalAmortizationDetails
    {
        public double BrokenPeriodDailyRate { get; set; }
        public double DailyRate { get; set; }
        public double FrequencyRate { get; set; }
        public DateTime? LoanEndDate { get; set; }
        public DateTime? LoanOriginalEndDate { get; set; }
        public int BrokenPeriodDays { get; set; }
        public int PeriodDays { get; set; }
        public int TermBasedOnLoanEndDate { get; set; }
    }
}