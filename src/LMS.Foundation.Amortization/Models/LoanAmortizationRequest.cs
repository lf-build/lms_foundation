﻿using System;
using System.Collections.Generic;

namespace LMS.Foundation.Amortization
{
    public class LoanAmortizationRequest : ILoanAmortizationRequest
    {
        public string PortfolioType { get; set; }
        public double LoanAmount { get; set; }
        public double PaymentAmount { get; set; }
        public double InterestRate { get; set; }
        public PaymentFrequency PaymentFrequency { get; set; }
        public int Term { get; set; }
        public DateTime PaymentStartDate { get; set; }
        public DateTime OriginalStartDate { get; set; }
        public DateTime LoanStartDate { get; set; }
        public double FactorRate { get; set; }
        public string RoundingMethod { get; set; }
        public int RoundingDigit { get; set; }
        public List<PaybackTier> PaybackTier { get; set; }
        public List<PaymentStream> PaymentStream { get; set; }
        public bool IsHolidaySchedule { get; set; }
        public bool IsDoubleScheduleForHoliday { get; set; }
        public InterestAdjustmentType InterestAdjustmentType { get; set; }
        public double RemainingInterestAmount { get; set; }
        public InterestHandlingType InterestHandlingType { get; set; }
        public List<SlabDetails> SlabDetails { get; set; }
        public int TermInMonth { get; set; }
        public int NumberOfDaysInMonth { get; set; }
        public int NumberOfDaysInWeek { get; set; }
        public string ActualAmortizationType { get; set; }
        public string DailyAccrualMethod { get; set; }
        public string MaturityTerm { get; set; }
        /// <summary>
        /// If set to InterestOnlyFollowedByBalloon, interest only schedule will be generated
        /// with a final repayment of principal in the last instalment
        /// </summary>
        public ScheduleType ScheduleType { get; set; }
        public double MADPercentage { get; set; }
        /// <summary>
        /// NumberOfDaysInYear will be used to calculate daily rate for SCF product
        /// </summary>
        public int NumberOfDaysInYear { get; set; }
    }
}
