﻿using System;

namespace LMS.Foundation.Amortization
{
    public class LoanScheduleDetail : ILoanScheduleDetail
    {
        public int Installmentnumber { get; set; }
        public DateTime ScheduleDate { get; set; }
        public DateTime OriginalScheduleDate { get; set; }

        public double PaymentAmount { get; set; }
        public double InterestAmount { get; set; }
        public double SurplusAmount { get; set; }

        public double PrincipalAmount { get; set; }
        public double CumulativeInterestAmount { get; set; }
        public double CumulativePrincipalAmount { get; set; }
        public double CumulativePaymentAmount { get; set; }        
        public double OpeningPrincipalOutStandingBalance { get; set; }
        public double ClosingPrincipalOutStandingBalance { get; set; }
        public InterestHandlingType InterestHandlingType { get; set; }
        public int FrequencyDays { get; set; }
        public double MinimumAmountDue { get; set; }

    }
}
