﻿namespace LMS.Foundation.Amortization
{
    public class PaybackTier : IPaybackTier
    {
        public int NoOfPayments { get; set; }
        public double Principal { get; set; }
        public double FinanceCharge { get; set; }
        public bool IsPercent { get; set; }
        internal PaymentFrequency PaymentFrequency { get; set; }
    }
}
