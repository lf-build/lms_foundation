﻿using System;
using System.Collections.Generic;

namespace LMS.Foundation.Amortization
{
    public class ScheduleInputDetails : IScheduleInputDetails
    {
        public double PrincipalOutStanding { get; set; }
        public DateTime PaymentStartDate { get; set; }
        public DateTime OriginalPaymentStartDate { get; set; }
        public DateTime OriginalStartDate { get; set; }
        public PaymentFrequency PaymentFrequency { get; set; }
        public string RoundingMethod { get; set; }
        public int RoundingDigit { get; set; }
        public int Term { get; set; }
        public InterestHandlingType InterestHandlingType { get; set; }
        public DateTime LoanStartDate { get; set; }
        public DateTime OriginalLoanStartDate { get; set; }
        public InterestAdjustmentType InterestAdjustmentType { get; set; }
        public double RemainingInterestAmount { get; set; }
        public List<PaymentStream> PaymentStream { get; set; }
        public int MaturityDays { get; set; }
        public string ActualAmortizationType { get; set; }
        public string DailyAccrualMethod { get; set; }
        /// <summary>
        /// If set to InterestOnlyFollowedByBalloon, interest only schedule will be generated
        /// with a final repayment of principal in the last instalment
        /// </summary>
        public ScheduleType ScheduleType { get; set; }
        public double MADPercentage { get; set; }
        /// <summary>
        /// NumberOfDaysInYear will be used to calculate daily rate for SCF product
        /// </summary>
        public int NumberOfDaysInYear { get; set; }

    }
}