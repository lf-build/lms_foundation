﻿namespace LMS.Foundation.Amortization
{
    public interface IPaybackTier
    {
        int NoOfPayments { get; set; }
        double Principal { get; set; }
        double FinanceCharge { get; set; }
        bool IsPercent { get; set; }
    }
}
