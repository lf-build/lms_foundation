﻿using System;
using System.Collections.Generic;

namespace LMS.Foundation.Amortization
{
    public interface IScheduleInputDetails
    {
        double PrincipalOutStanding { get; set; }
        DateTime PaymentStartDate { get; set; }
        DateTime OriginalPaymentStartDate { get; set; }
        DateTime OriginalStartDate { get; set; }
        DateTime OriginalLoanStartDate { get; set; }
        PaymentFrequency PaymentFrequency { get; set; }
        string RoundingMethod { get; set; }
        int RoundingDigit { get; set; }
        int Term { get; set; }
        InterestAdjustmentType InterestAdjustmentType { get; set; }
        double RemainingInterestAmount { get; set; }
        InterestHandlingType InterestHandlingType { get; set; }
        DateTime LoanStartDate { get; set; }
        List<PaymentStream> PaymentStream { get; set; }
        int MaturityDays { get; set; }
        string ActualAmortizationType { get; set; }
        string DailyAccrualMethod { get; set; }
        /// <summary>
        /// If set to InterestOnlyFollowedByBalloon, interest only schedule will be generated
        /// with a final repayment of principal in the last instalment
        /// </summary>
        ScheduleType ScheduleType { get; set; }
        double MADPercentage { get; set; }
        /// <summary>
        /// NumberOfDaysInYear will be used to calculate daily rate for SCF product
        /// </summary>
        /// <value></value>
        int NumberOfDaysInYear { get; set; }

    }
}
