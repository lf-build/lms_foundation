﻿namespace LMS.Foundation.Amortization
{
    public interface IPaymentStream
    {
        int NoOfPayments { get; set; }
        PaymentFrequency PaymentFrequency { get; set; }
        int RepayablePercent { get; set; }
    }
}