﻿using System;
using System.Collections.Generic;

namespace LMS.Foundation.Amortization
{
    public interface ILoanAmortizationRequest
    {       
        string PortfolioType { get; set; }
        double LoanAmount { get; set; }
        double InterestRate { get; set; }
        double PaymentAmount { get; set; }
        PaymentFrequency PaymentFrequency { get; set; }
        int Term { get; set; }
        DateTime PaymentStartDate { get; set; }
        DateTime OriginalStartDate { get; set; }
        DateTime LoanStartDate { get; set; }
        double FactorRate { get; set; }
        string RoundingMethod { get; set; }
        int RoundingDigit { get; set; }
        List<PaybackTier> PaybackTier { get; set; }
        List<PaymentStream> PaymentStream { get; set; }
        bool IsHolidaySchedule { get; set; }
        bool IsDoubleScheduleForHoliday { get; set; }
        InterestAdjustmentType InterestAdjustmentType { get; set; }
        double RemainingInterestAmount { get; set; }
        InterestHandlingType InterestHandlingType { get; set; }
        List<SlabDetails> SlabDetails { get; set; }
        int TermInMonth { get; set; }
        int NumberOfDaysInMonth { get; set; }
        int NumberOfDaysInWeek { get; set; }
        string ActualAmortizationType{ get; set; }
        string DailyAccrualMethod{ get; set; }
        string MaturityTerm{ get; set; }
        /// <summary>
        /// If set to InterestOnlyFollowedByBalloon, interest only schedule will be generated
        /// with a final repayment of principal in the last instalment
        /// </summary>
        ScheduleType ScheduleType { get; set; }
        double MADPercentage { get; set; }
        /// <summary>
        /// NumberOfDaysInYear will be used to calculate daily rate for SCF product
        /// </summary>
        int NumberOfDaysInYear { get; set; }

    }
}
