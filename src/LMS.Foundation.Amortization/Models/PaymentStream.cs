﻿
namespace LMS.Foundation.Amortization
{
    public class PaymentStream :IPaymentStream
    {
        public int RepayablePercent { get; set; }
        public PaymentFrequency PaymentFrequency { get; set; }
        public int NoOfPayments { get; set; }
        internal double AppliedInterestRate { get; set; }
        internal double InstallmentAmount { get; set; }
    }
}
