﻿using System;

namespace LMS.Foundation.Amortization
{
    public interface ILastScheduleDetails
    {
        int InstallmentNumber { get; set; }
        DateTime LastScheduleDate { get; set; }
        DateTime LastNonHolidayScheduleDate { get; set; }
        double CumulativeInterestAmount { get; set; }
        double CumulativePrincipalAmount { get; set; }
        double CumulativePaymentAmount { get; set; }
        double LastPrincipalOutStandingBalance { get; set; }

    }
}
