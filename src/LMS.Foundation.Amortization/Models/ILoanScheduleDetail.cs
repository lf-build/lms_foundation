﻿using System;

namespace LMS.Foundation.Amortization
{
    public interface ILoanScheduleDetail
    {
        int Installmentnumber { get; set; }
        DateTime ScheduleDate { get; set; }
        DateTime OriginalScheduleDate { get; set; }
        double PaymentAmount { get; set; }
        double InterestAmount { get; set; }
        double SurplusAmount { get; set; }
        double PrincipalAmount { get; set; }
        double CumulativeInterestAmount { get; set; }
        double CumulativePrincipalAmount { get; set; }
        double CumulativePaymentAmount { get; set; }
        double OpeningPrincipalOutStandingBalance { get; set; }
        double ClosingPrincipalOutStandingBalance { get; set; }
        InterestHandlingType InterestHandlingType { get; set; }
        int FrequencyDays { get; set; }
        double MinimumAmountDue { get; set; }


    }
}
