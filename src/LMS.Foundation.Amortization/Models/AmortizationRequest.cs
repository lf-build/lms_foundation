﻿using System;
using System.Collections.Generic;

namespace LMS.Foundation.Amortization
{
    public class AmortizationRequest : IAmortizationRequest
    {

        /// <summary>
        /// ActualAmortizationType
        /// </summary>
        /// <value>VariableDailyRate or FixDailyRate</value>
        public string ActualAmortizationType { get; set; }
        /// <summary>
        /// DailyAccrualMethod
        /// </summary>
        /// <value>30/360 or Actual/Actual or Actual/365</value>
        public string DailyAccrualMethod { get; set; }
        /// <summary>
        /// BrokenPeriodInterestType
        /// </summary>
        /// <value>30/360 or Actual/Actual or Actual/365 or Variable</value>
        public string BrokenPeriodInterestType { get; set; }
        /// <summary>
        /// BrokenPeriodHandlingType
        /// </summary>
        /// <value>Separate or FirstEmi or FullEmi or Ignore</value>
        public string BrokenPeriodHandlingType { get; set; }
        /// <summary>
        /// If set to InterestOnlyFollowedByBalloon, interest only schedule will be generated
        /// with a final repayment of principal in the last instalment
        /// </summary>
        /// <value>Separate or FirstEmi or FullEmi or Ignore</value>
        public ScheduleType ScheduleType { get; set; }
        /// <summary>
        /// CarryForwardOutstanding
        /// </summary>
        /// <value>PrincipalOnly or PrincipalInterest</value>
        public string CarryForwardOutstanding { get; set; }
        /// <summary>
        /// RoundingMethod
        /// </summary>
        /// <value>mathematical or bankers</value>
        public string RoundingMethod { get; set; }
        /// <summary>
        /// RoundingDigit
        /// </summary>
        /// <value>any int value like 1, 2, 3...</value>
        public int RoundingDigit { get; set; }
        /// <summary>
        /// IsHolidaySchedule
        /// </summary>
        /// <value>true or false</value>
        public bool IsHolidaySchedule { get; set; }
        /// <summary>
        /// IsDoubleScheduleForHoliday
        /// </summary>
        /// <value>true or false</value>
        public bool IsDoubleScheduleForHoliday { get; set; }
        /// <summary>
        /// BillingDate
        /// </summary>
        /// <value>any int value like 1, 2, 3...</value>
        public int BillingDate { get; set; }
        /// <summary>
        /// PortfolioType
        /// </summary>
        /// <value>Installment or MCA or LineOfCredit or MCALOC or SCF</value>
        public string PortfolioType { get; set; }
        /// <summary>
        /// LoanAmount
        /// </summary>
        /// <value>Any double value like 10000, 20000, 30000...</value>
        public double LoanAmount { get; set; }
        /// <summary>
        /// PaymentAmount
        /// </summary>
        /// <value>Any double value like 1000, 2000, 3000...</value>
        public double PaymentAmount { get; set; }
        /// <summary>
        /// InterestRate
        /// </summary>
        /// <value>Any double value like 20, 20.5, 21, 21.5...</value>
        public double InterestRate { get; set; }
        /// <summary>
        /// FactorRate
        /// </summary>
        /// <value>Any double value like 1, 1.2, 1.5, 1.8...</value>
        public double FactorRate { get; set; }
        /// <summary>
        /// PaymentFrequency
        /// </summary>
        /// <value>Daily or Weekly or BiWeekly or Monthly or Quarterly or Yearly or SemiYearly</value>
        public PaymentFrequency PaymentFrequency { get; set; }
        /// <summary>
        /// Term
        /// </summary>
        /// <value>Any int value like 6, 10, 12...</value>
        public int Term { get; set; }
        /// <summary>
        /// FundedDate
        /// </summary>
        /// <value></value>
        public DateTime FundedDate { get; set; }
        /// <summary>
        /// FirstPaymentDate
        /// </summary>
        /// <value></value>
        public DateTime FirstPaymentDate { get; set; }
        /// <summary>
        /// OriginalFirstPaymentDate
        /// </summary>
        /// <value></value>
        public DateTime OriginalFirstPaymentDate { get; set; }
        /// <summary>
        /// PaybackTier
        /// </summary>
        /// <value></value>
        public List<PaybackTier> PaybackTier { get; set; }
        /// <summary>
        /// PaymentStream
        /// </summary>
        /// <value></value>
        public List<PaymentStream> PaymentStream { get; set; }
        /// <summary>
        /// MaturityTermDays
        /// </summary>
        /// <value>Any int value like 30, 60, 90...</value>
        public int MaturityTermDays { get; set; }

    }
}
