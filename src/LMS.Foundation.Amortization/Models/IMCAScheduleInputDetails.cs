﻿using System;
using System.Collections.Generic;

namespace LMS.Foundation.Amortization
{
    public interface IMCAScheduleInputDetails
    {
        double PrincipalOutStanding { get; set; }
        int Term { get; set; }
        DateTime PaymentStartDate { get; set; }
        DateTime OriginalStartDate { get; set; }
        PaymentFrequency PaymentFrequency { get; set; }
        string RoundingMethod { get; set; }
        int RoundingDigit { get; set; }
        List<PaybackTier> PaybackTier { get; set; }
        List<IPaymentStream> PaymentStream { get; set; }
        double LoanAmount { get; set; }
        InterestAdjustmentType InterestAdjustmentType { get; set; }
        double RemainingInterestAmount { get; set; }
        InterestHandlingType InterestHandlingType { get; set; }
        DateTime LoanStartDate { get; set; }
        DateTime OriginalLoanStartDate { get; set; }
    }
}
