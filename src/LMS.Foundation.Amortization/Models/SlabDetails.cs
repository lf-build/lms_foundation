﻿namespace LMS.Foundation.Amortization
{
    public class SlabDetails
    {
        public int MonthSequence { get; set; }
        public int Percentage{ get; set; }
    }
}