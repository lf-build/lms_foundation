﻿using System;

namespace LMS.Foundation.Amortization
{
    public class LastScheduleDetails : ILastScheduleDetails
    {
        public int InstallmentNumber { get; set; }
        public DateTime LastScheduleDate { get; set; }
        public DateTime LastNonHolidayScheduleDate { get; set; }
        public double CumulativeInterestAmount { get; set; }
        public double CumulativePrincipalAmount { get; set; }
        public double CumulativePaymentAmount { get; set; }        
        public double LastPrincipalOutStandingBalance { get; set; }
       

    }
}
