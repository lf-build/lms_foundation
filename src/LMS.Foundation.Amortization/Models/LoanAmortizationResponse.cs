﻿using System.Collections.Generic;

namespace LMS.Foundation.Amortization
{
    public class LoanAmortizationResponse : ILoanAmortizationResponse
    {    
       public List<ILoanScheduleDetail> LoanScheduleDetail { get; set; }

        public List<ILoanScheduleDetail> DailyLoanScheduleDetail { get; set; }

    }
}
