﻿using System;
using System.Collections.Generic;

namespace LMS.Foundation.Amortization
{
    public class MCAScheduleInputDetails : IMCAScheduleInputDetails
    {
        public double PrincipalOutStanding { get; set; }
        public int Term { get; set; }
        public DateTime PaymentStartDate { get; set; }
        public DateTime OriginalStartDate { get; set; }
        public PaymentFrequency PaymentFrequency { get; set; }
        public string RoundingMethod { get; set; }
        public int RoundingDigit { get; set; }
        public List<PaybackTier> PaybackTier { get; set; }
        public List<IPaymentStream> PaymentStream { get; set; }
        public double LoanAmount { get; set; }
        public InterestAdjustmentType InterestAdjustmentType { get; set; }
        public double RemainingInterestAmount { get; set; }
       public InterestHandlingType InterestHandlingType { get; set; }
       public DateTime LoanStartDate { get; set; }
        public DateTime OriginalLoanStartDate { get; set; }
        
    }
}
