﻿namespace LMS.Foundation.Amortization
{
    public class AmortizationResponse
    {
        public double PaymentAmount { get; set; }
        public double InterestAmount { get; set; }
        public  double PrincipalAmount { get; set; }
        public  double TotalPaybackAmount { get; set; }
        public  double InstallmentAmount{ get; set; }
        internal int SequenceNumber{ get; set; }
    }
}