﻿using System.Collections.Generic;

namespace LMS.Foundation.Amortization
{
    public interface ILoanAmortizationResponse
    {
        List<ILoanScheduleDetail> LoanScheduleDetail { get; set; }
        List<ILoanScheduleDetail> DailyLoanScheduleDetail { get; set; }
    }
}
