﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMS.Foundation.Amortization
{
    public interface ILoanAmortization
    {
       ILoanAmortizationResponse GetAmortizationScedule(ILoanAmortizationRequest loanAmortizationRequest);
       ILoanAmortizationResponse CalculateCumulativeForSchedule(List<ILoanScheduleDetail> SchduleList, double loanAmount, string roundingMethod, int roundingDigit);
       List<AmortizationResponse> GetFcAmortization(ILoanAmortizationRequest customRequest);
       ILoanAmortizationResponse GetAmortizationSchedule(IAmortizationRequest amortizationRequest);
    }
}
