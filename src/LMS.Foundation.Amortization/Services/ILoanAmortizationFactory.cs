﻿using LendFoundry.Security.Tokens;

namespace LMS.Foundation.Amortization
{
    public interface ILoanAmortizationFactory
    {
        ILoanAmortization Create(ITokenReader reader);
    }
}