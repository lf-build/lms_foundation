﻿using LendFoundry.Calendar.Client;
using LendFoundry.Security.Tokens;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace LMS.Foundation.Amortization
{
    public class LoanAmortizationFactory : ILoanAmortizationFactory
    {


        public LoanAmortizationFactory(IServiceProvider provider)
        {
            if (provider == null) throw new ArgumentException($"{nameof(provider)} cannot be null");

            Provider = provider;
        }

        private IServiceProvider Provider { get; }
        public ILoanAmortization Create(ITokenReader reader)
        {

            var calendarServiceFactory = Provider.GetService<ICalendarServiceFactory>();
            var calendar = calendarServiceFactory.Create(reader);

            return new LoanAmortization(calendar);
        }
    }
}
