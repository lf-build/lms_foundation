﻿using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
namespace LMS.Foundation.Amortization
{
    public static class AmortizationExtension
    {
        public static IServiceCollection AddAmortization(this IServiceCollection services)
        {
            services.AddTransient<ILoanAmortizationFactory, LoanAmortizationFactory>();
            services.AddTransient(p => p.GetService<ILoanAmortizationFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
