﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using LendFoundry.Calendar.Client;
using LMS.Foundation.Calculus;

namespace LMS.Foundation.Amortization
{
    internal partial class LoanAmortization : ILoanAmortization
    {
        #region Constructor

        public LoanAmortization() { }

        public LoanAmortization(ICalendarService calendarService)
        {
            CalendarService = calendarService;
        }

        #endregion

        #region Variables

        private ICalendarService CalendarService { get; }
        private DateTime StoredDate { get; set; }
        private bool IsStored { get; set; }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="loanAmortizationRequest">
        /// PortfolioType - mandatory, values can be
        /// - Installment
        /// - Mortgage
        /// - Open
        /// - Revolving
        /// - LOC
        /// - MCALOC
        /// LoanAmount - Amount of the loan in loan currency
        /// - has to be > 0
        /// InterestRate - the annual interest rate
        /// PaymentAmount - the loan amount
        /// PaymentFrequency - mandatory, values can be
        /// - Daily (D)
        /// - Weekly (W)
        /// - BiWeekly (F)
        /// - Monthly (M)
        /// - Quarterly (Q)
        /// - SemiYearly (H)
        /// - Yearly (Y)
        /// Term TODO what is the unit of measure for term?
        /// PaymentStartDate TODO what is this date?
        /// OriginalStartDate TODO what is this date?
        /// LoanStartDate - The date on which the loan is opened?
        /// FactorRate - mandatory for MCA, MCALOC in FC Scenario. Must be > 0
        /// RoundingMethod TODO what are the options?
        /// RoundingDigit TODO what are the options?
        /// PaybackTier TODO what is this?
        /// PaymentStream TODO what is this?
        /// IsHolidaySchedule TODO what is this?
        /// IsDoubleScheduleForHoliday TODO what is this?
        /// InterestAdjustmentType TODO what is this?
        /// RemainingInterestAmount TODO what is this?
        /// InterestHandlingType TODO what is this?
        /// SlabDetails - optional, a list containing
        /// - month sequence
        /// - percentage
        /// If sum(percentage) is hundred, it is treated as FC Scenario.
        /// In that case, the SlabDetails count must match the TermInMonth
        /// TermInMonth - mandatory for MCA, MCALOC in FC scenario. Must be > 0 and must match the SlabDetails count
        /// NumberOfDaysInMonth TODO what is the purpose?
        /// NumberOfDaysInWeek TODO what is the purpose?
        /// ActualAmortizationType 
        /// - VariableDailyRate - daily rate for accrual will change based based on frequency rate
        /// - FixedDailyRate - daily rate will be based on actual no of days in year
        /// DailyAccrualMethod
        /// - 30/360
        /// - Actual/Actual
        /// </param>
        /// <returns>ILoanAmortizationResponse
        /// Which is a list of the following:
        /// Installmentnumber - Installment number
        /// ScheduleDate - Schedule Date
        /// OriginalScheduleDate - Original Schedule Date - TODO what is this?
        /// PaymentAmount - payment amount
        /// InterestAmount - interest amount
        /// SurplusAmount - surplus amount
        /// PrincipalAmount - principal amount
        /// CumulativeInterestAmount - cumulative interest amount
        /// CumulativePrincipalAmount - cumulative principal amount
        /// CumulativePaymentAmount - cumulative payment amount
        /// OpeningPrincipalOutStandingBalance - opening principal outstanding balance
        /// ClosingPrincipalOutStandingBalance - closing principal outstanding balance
        /// InterestHandlingType - interest handling type TODO what is this?
        /// FrequencyDays - frequency days TODO what is this?
        /// </returns>
        /// <exception cref="InvalidOperationException"></exception>
        public ILoanAmortizationResponse GetAmortizationScedule(ILoanAmortizationRequest loanAmortizationRequest)
        {
            try
            {
                ValidateRequest(loanAmortizationRequest);
                switch (GetPortfolioType(loanAmortizationRequest.PortfolioType))
                {
                    case PortfolioType.MCA:
                    case PortfolioType.MCALOC:
                        if (isFCScenario(loanAmortizationRequest.SlabDetails))
                        {
                            return new LoanAmortizationResponse
                            {
                                LoanScheduleDetail = GetFCSchedule(loanAmortizationRequest)
                            };
                        }
                        return new LoanAmortizationResponse
                        {
                            LoanScheduleDetail = GetMCASchedule(GetMCAParameters(loanAmortizationRequest),
                                loanAmortizationRequest.IsHolidaySchedule,
                                loanAmortizationRequest.IsDoubleScheduleForHoliday)
                        };
                    case PortfolioType.Installment:
                    case PortfolioType.LineOfCredit:
                    case PortfolioType.Mortgage:
                    case PortfolioType.Open:
                    case PortfolioType.Revolving:
                    case PortfolioType.SCF:
                        switch (loanAmortizationRequest.ScheduleType)
                        {
                            case ScheduleType.InterestOnlyFollowedByBalloon:
                                var balloonResponse = GetInterestOnlyFollowedByBalloonActualSchedule(
                                    GetInterestOnlyFollowedByBalloonRequiredParameter(loanAmortizationRequest),
                                    loanAmortizationRequest.InterestRate, loanAmortizationRequest.PortfolioType);
                                return new LoanAmortizationResponse
                                {
                                    LoanScheduleDetail = balloonResponse.Item1,
                                    DailyLoanScheduleDetail = balloonResponse.Item2
                                };
                            case ScheduleType.EquatedMonthlyInstalments:
                            case ScheduleType.None:
                            default:
                                if (loanAmortizationRequest.ActualAmortizationType == "VariableDailyRate" ||
                                    loanAmortizationRequest.DailyAccrualMethod == "30/360")
                                {

                                    return new LoanAmortizationResponse
                                    {
                                        LoanScheduleDetail = GetSchedule(GetRequiredParameters(loanAmortizationRequest),
                                    loanAmortizationRequest.IsHolidaySchedule, loanAmortizationRequest.PortfolioType)
                                    };
                                }

                                var response = GetActualSchedule(GetRequiredParameters(loanAmortizationRequest),
                                    loanAmortizationRequest.IsHolidaySchedule, loanAmortizationRequest.InterestRate, loanAmortizationRequest.PortfolioType);
                                return new LoanAmortizationResponse
                                {
                                    LoanScheduleDetail = response.Item1,
                                    DailyLoanScheduleDetail = response.Item2
                                };
                        }

                    default:
                        throw new InvalidEnumArgumentException(
                            $"Invalid PortfolioType : {loanAmortizationRequest.PortfolioType}");
                }
            }
            catch (Exception ex)
            {
                // TODO we need a Logger here
                // Logger.Error(ex.Message, ex, new {loanAmortizationRequest});
                throw new InvalidOperationException(ex.Message, ex);
            }
        }

        /// <summary>
        /// For FC Amortization, ie
        /// MCA or MCALOC
        /// With SlabDetails Percentage totalling 100:
        /// principal amount is LoanAmount / TermInMonth
        /// repayment is principal amount times the FactorRate
        /// Example:
        /// Loan amount : 10000
        /// Term in months : 10
        /// Factor Rate : 1.4
        /// SlabDetails : [ {1, 20%}, {2, 30%}, {3, 25%}, {4, 2%}, {5, 2%}, {6, 1%}, {7, 5%}, {8, 5%}, {9, 5%}, {10, 5%}]
        /// number of slabs is equal to number of repayments, and sum of percentages is 100
        ///
        /// The principal amount per instalment is 10000/10 = 1000
        /// Instalment 1 : 
        /// The interest amount is (10000 x 1.4 - 10000) x Percentage for instalment 1 (20%) = 4000 x .2 = 800
        /// Instalment 2 :  
        /// The interest amount is (10000 x 1.4 - 10000) x Percentage for instalment 2 (30%) = 4000 x .3 = 1200
        /// and so on
        /// Instalment 10 :  
        /// The interest amount is (10000 x 1.4 - 10000) x Percentage for instalment 10 (5%) = 4000 x .05 = 20
        ///
        /// </summary>
        /// <param name="customRequest"></param>
        /// <returns>
        /// The output is of the form
        /// [Sequence Number] [Principal Amount]  [Interest Amount] [Payment Amount ] [Total Payback Amount]
        /// 1                   1000                  800                 1800         10000 + 800 = 10800
        /// 2                   1000                 1200                 2200         10000 + 800 + 1200 = 12000
        /// and so on
        /// 10                  1000                   20                 1020         10000 + total interest
        ///
        /// InstallmentAmount is NOT populated. It is expected that the caller will populate it after getting response.
        /// 
        /// Care is taken to round off only once in the end just before saving to the response array.
        /// </returns>
        public List<AmortizationResponse> GetFcAmortization(ILoanAmortizationRequest customRequest)
        {

            ValidateFCInput(customRequest);

            var principalAmtPerInstalment = customRequest.LoanAmount / customRequest.TermInMonth;

            var principalAmtPerInstalmentRoundedOff =
                RoundOff.Round(principalAmtPerInstalment,
                    customRequest.RoundingDigit, customRequest.RoundingMethod);
            var totalInterest = customRequest.LoanAmount * (1 - customRequest.FactorRate);
            var cumPaybackAmt = customRequest.LoanAmount;

            var amortizationResponse = new List<AmortizationResponse>();
            for (var i = 1; i <= customRequest.TermInMonth; i++)
            {
                var slab = customRequest.SlabDetails.FirstOrDefault(x => x.MonthSequence == i);
                if (null == slab)
                {
                    throw new Exception($"Unable to find slab for month [{i}]");
                }

                var interestAmt = totalInterest * slab.Percentage / 100;
                cumPaybackAmt += interestAmt;

                var amortization = new AmortizationResponse
                {
                    SequenceNumber = i,
                    PrincipalAmount = principalAmtPerInstalmentRoundedOff,
                    InterestAmount = RoundOff.Round(interestAmt,
                    customRequest.RoundingDigit, customRequest.RoundingMethod),
                    PaymentAmount = RoundOff.Round(principalAmtPerInstalment + interestAmt,
                    customRequest.RoundingDigit, customRequest.RoundingMethod),
                    TotalPaybackAmount =
                    RoundOff.Round(cumPaybackAmt, customRequest.RoundingDigit, customRequest.RoundingMethod),
                };

                amortizationResponse.Add(amortization);
            }

            return amortizationResponse;
        }

        /// <summary>
        /// This method will use for Loanschedule migration
        /// </summary>
        /// <param name="SchduleList">LoanScheduleDetails</param>
        /// <param name="loanAmount">LoanAmount available in LoanscheduleRequest</param>
        /// <param name="roundingMethod">Rounding method name</param>
        /// <param name="roundingDigit">Number of digits for round the amount</param>
        /// <returns>LoanScheduleDetail with calculated cumulative fields</returns>
        public ILoanAmortizationResponse CalculateCumulativeForSchedule(List<ILoanScheduleDetail> SchduleList, double loanAmount, string roundingMethod, int roundingDigit)
        {
            ILoanScheduleDetail lastSchdeuleDetail = new LoanScheduleDetail();

            foreach (var scheduleDetails in SchduleList)
            {
                scheduleDetails.Installmentnumber = lastSchdeuleDetail.Installmentnumber + 1;
                //Cumulative details
                scheduleDetails.CumulativeInterestAmount = RoundOff.Round(lastSchdeuleDetail.CumulativeInterestAmount + scheduleDetails.InterestAmount, roundingDigit, roundingMethod);
                scheduleDetails.CumulativePrincipalAmount = RoundOff.Round(lastSchdeuleDetail.CumulativePrincipalAmount + scheduleDetails.PrincipalAmount, roundingDigit, roundingMethod);
                scheduleDetails.CumulativePaymentAmount = RoundOff.Round(lastSchdeuleDetail.CumulativePaymentAmount + scheduleDetails.PaymentAmount, roundingDigit, roundingMethod);
                scheduleDetails.OpeningPrincipalOutStandingBalance = RoundOff.Round(scheduleDetails.Installmentnumber == 1 ? loanAmount : lastSchdeuleDetail.ClosingPrincipalOutStandingBalance, roundingDigit, roundingMethod);
                scheduleDetails.ClosingPrincipalOutStandingBalance = RoundOff.Round(scheduleDetails.OpeningPrincipalOutStandingBalance - scheduleDetails.PrincipalAmount, roundingDigit, roundingMethod); ;
                lastSchdeuleDetail = scheduleDetails;
            }
            return new LoanAmortizationResponse
            {
                LoanScheduleDetail = SchduleList
            };
        }

        #region Private Methods

        #region MCA

        private IMCAScheduleInputDetails GetMCAParameters(ILoanAmortizationRequest loanAmortizationRequest)
        {
            if (loanAmortizationRequest.PaybackTier == null || loanAmortizationRequest.PaybackTier.Count == 0)
            {
                throw new Exception("PaybackTier is required to GetMCAParameters");
            }

            var paybackTiers = new List<PaybackTier>();
            if (loanAmortizationRequest.PaymentStream != null)
            {
                foreach (var paybackStream in loanAmortizationRequest.PaymentStream)
                {
                    var paybackTier = new PaybackTier
                    {
                        NoOfPayments = paybackStream.NoOfPayments,
                        Principal = paybackStream.RepayablePercent * loanAmortizationRequest.LoanAmount / 100,
                        IsPercent = false,
                        PaymentFrequency = paybackStream.PaymentFrequency
                    };
                    paybackTier.FinanceCharge = (paybackTier.Principal * loanAmortizationRequest.FactorRate) -
                        paybackTier.Principal;
                    paybackTiers.Add(paybackTier);
                }
            }

            PaybackTier pbt;
            switch (loanAmortizationRequest.InterestAdjustmentType)
            {
                case InterestAdjustmentType.FirstInterestOnlySchedule:
                    pbt = loanAmortizationRequest.PaymentStream != null ?
                        paybackTiers.First() :
                        loanAmortizationRequest.PaybackTier.First();
                    pbt.NoOfPayments -= 1;
                    break;
                case InterestAdjustmentType.LastInterestOnlySchedule:
                    pbt = loanAmortizationRequest.PaymentStream != null ?
                        paybackTiers.Last() :
                        loanAmortizationRequest.PaybackTier.Last();
                    pbt.NoOfPayments -= 1;
                    break;
                case InterestAdjustmentType.None:
                case InterestAdjustmentType.DistributedSchedule:
                case InterestAdjustmentType.FirstSchedule:
                case InterestAdjustmentType.LastSchedule:
                    break;
                default:
                    throw new Exception("Invalid InterestAdjustmentType when getting MCA Parameters");
            }

            loanAmortizationRequest.PaybackTier.ForEach(p => { p.PaymentFrequency = loanAmortizationRequest.PaymentFrequency; });

            var paymentStartDate = CalendarService.GetDate(
                loanAmortizationRequest.PaymentStartDate.Year, loanAmortizationRequest.PaymentStartDate.Month,
                loanAmortizationRequest.PaymentStartDate.Day);
            var loanStartDate = CalendarService.GetDate(
                loanAmortizationRequest.LoanStartDate.Year, loanAmortizationRequest.LoanStartDate.Month,
                loanAmortizationRequest.LoanStartDate.Day);
            return new MCAScheduleInputDetails
            {
                PrincipalOutStanding = RoundOff.Round(
                        loanAmortizationRequest.LoanAmount * loanAmortizationRequest.FactorRate,
                        loanAmortizationRequest.RoundingDigit, loanAmortizationRequest.RoundingMethod),
                Term = loanAmortizationRequest.InterestAdjustmentType == InterestAdjustmentType.LastInterestOnlySchedule ?
                    loanAmortizationRequest.Term - 1 : loanAmortizationRequest.Term,
                PaymentStartDate = paymentStartDate.Type == DateType.Holiday || paymentStartDate.Type == DateType.Weekend ?
                    paymentStartDate.NextBusinessDay.Date : loanAmortizationRequest.PaymentStartDate,
                OriginalStartDate = loanAmortizationRequest.OriginalStartDate != DateTime.MinValue ?
                    loanAmortizationRequest.OriginalStartDate : loanAmortizationRequest.PaymentStartDate,
                PaymentFrequency = loanAmortizationRequest.PaymentFrequency,
                RoundingMethod = loanAmortizationRequest.RoundingMethod,
                RoundingDigit = loanAmortizationRequest.RoundingDigit,
                PaybackTier = loanAmortizationRequest.PaymentStream != null ?
                    paybackTiers :
                    loanAmortizationRequest.PaybackTier,
                // PaymentStream - is not populated, because it has been converted to PaybackTier
                LoanAmount = loanAmortizationRequest.LoanAmount,
                InterestAdjustmentType = loanAmortizationRequest.InterestAdjustmentType,
                RemainingInterestAmount = loanAmortizationRequest.RemainingInterestAmount,
                InterestHandlingType = loanAmortizationRequest.InterestHandlingType,
                LoanStartDate = loanStartDate.Type == DateType.Holiday || loanStartDate.Type == DateType.Weekend ?
                    loanStartDate.NextBusinessDay.Date : loanAmortizationRequest.LoanStartDate,
                OriginalLoanStartDate = loanStartDate.Date.Date
            };
        }

        /* Refactored code was not working, hence replacing with older method. Need to get checked by Foram */
        private List<ILoanScheduleDetail> GetMCASchedule(IMCAScheduleInputDetails scheduleInputDetails, bool IsHolidaySchedule, bool IsDoubleScheduleOnHoliday)
        {
            var SchduleList = new List<LoanScheduleDetail>();
            var lastSchdeuleDetail = new LastScheduleDetails();
            InitializeMCAScheduleDetails(scheduleInputDetails, lastSchdeuleDetail);
            var RepayAmount = scheduleInputDetails.PrincipalOutStanding;
            var FinanceCharge = scheduleInputDetails.PrincipalOutStanding - scheduleInputDetails.LoanAmount;
            if (scheduleInputDetails.InterestAdjustmentType == InterestAdjustmentType.FirstInterestOnlySchedule)
            {
                SchduleList.Add(AddFirstInterestOnlyPayment(lastSchdeuleDetail, scheduleInputDetails.RemainingInterestAmount, lastSchdeuleDetail.LastPrincipalOutStandingBalance, scheduleInputDetails.PaymentFrequency, scheduleInputDetails.OriginalStartDate, scheduleInputDetails.RoundingMethod, scheduleInputDetails.RoundingDigit));
                UpdateLastScheduleInfo(lastSchdeuleDetail, SchduleList.FirstOrDefault());
            }
            foreach (var payback in scheduleInputDetails.PaybackTier)
            {
                for (int i = 0; i < payback.NoOfPayments; i++)
                {
                    var scheduleDetails = new LoanScheduleDetail();
                    scheduleDetails.Installmentnumber = lastSchdeuleDetail.InstallmentNumber;

                    if (!IsDoubleScheduleOnHoliday)
                    {
                        var scheduleDate = Tuple.Create(lastSchdeuleDetail.LastNonHolidayScheduleDate, lastSchdeuleDetail.LastNonHolidayScheduleDate, false);
                        scheduleDate = lastSchdeuleDetail.InstallmentNumber == 1 ? scheduleDate : GetNextSchedule(payback.PaymentFrequency, lastSchdeuleDetail.LastNonHolidayScheduleDate);
                        if (SchduleList.Any(x => x.ScheduleDate.Date == scheduleDate.Item1.Date))
                        {
                            IsStored = false;
                            scheduleDate = GetNextSchedule(payback.PaymentFrequency, scheduleDate.Item1.Date);
                        }
                        scheduleDetails.ScheduleDate = lastSchdeuleDetail.InstallmentNumber == 1 ? scheduleInputDetails.PaymentStartDate : (IsHolidaySchedule ? GetNextScheduleDate(payback.PaymentFrequency, lastSchdeuleDetail.LastNonHolidayScheduleDate) : scheduleDate.Item1);
                        var originalScheduleDate = lastSchdeuleDetail.InstallmentNumber == 1 && (scheduleInputDetails.OriginalStartDate != null || scheduleInputDetails.OriginalStartDate != DateTime.MinValue) ? scheduleInputDetails.OriginalStartDate : (IsHolidaySchedule ? GetNextScheduleDate(payback.PaymentFrequency, lastSchdeuleDetail.LastNonHolidayScheduleDate) : scheduleDate.Item2);
                        //daily 
                        scheduleDetails.OriginalScheduleDate = payback.PaymentFrequency == PaymentFrequency.Daily ? scheduleDetails.ScheduleDate : originalScheduleDate;
                    }
                    else
                    {

                        var scheduleDate = Tuple.Create(lastSchdeuleDetail.LastNonHolidayScheduleDate, lastSchdeuleDetail.LastNonHolidayScheduleDate, false);
                        if (payback.PaymentFrequency == PaymentFrequency.Daily)
                            IsStored = false;
                        scheduleDate = lastSchdeuleDetail.InstallmentNumber == 1 ? scheduleDate : GetNextSchedule(payback.PaymentFrequency, lastSchdeuleDetail.LastNonHolidayScheduleDate, IsDoubleScheduleOnHoliday);

                        scheduleDetails.ScheduleDate = lastSchdeuleDetail.InstallmentNumber == 1 ? scheduleInputDetails.PaymentStartDate : (IsHolidaySchedule ? GetNextScheduleDate(payback.PaymentFrequency, lastSchdeuleDetail.LastNonHolidayScheduleDate) : scheduleDate.Item1);
                        var originalScheduleDate = lastSchdeuleDetail.InstallmentNumber == 1 && (scheduleInputDetails.OriginalStartDate != null || scheduleInputDetails.OriginalStartDate != DateTime.MinValue) ? scheduleInputDetails.OriginalStartDate : (IsHolidaySchedule ? GetNextScheduleDate(payback.PaymentFrequency, lastSchdeuleDetail.LastNonHolidayScheduleDate) : scheduleDate.Item2);
                        //daily 
                        scheduleDetails.OriginalScheduleDate = scheduleDate.Item3 || lastSchdeuleDetail.InstallmentNumber == 1 ? originalScheduleDate : scheduleDetails.ScheduleDate;

                    }
                    scheduleDetails.OpeningPrincipalOutStandingBalance = RoundOff.Round(lastSchdeuleDetail.LastPrincipalOutStandingBalance, scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);

                    //payment details

                    scheduleDetails.InterestAmount = RoundOff.Round(GetInterestAmountForMCA(scheduleInputDetails, lastSchdeuleDetail, payback, FinanceCharge, SchduleList.Sum(x => x.SurplusAmount)), scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);
                    var additionalInterest = RoundOff.Round(GetInterestBasedOnAdjustType(scheduleInputDetails.InterestAdjustmentType, scheduleInputDetails.RemainingInterestAmount, scheduleInputDetails.Term, lastSchdeuleDetail.InstallmentNumber), scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);
                    scheduleDetails.InterestAmount = RoundOff.Round(scheduleDetails.InterestAmount + additionalInterest, scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);
                    scheduleDetails.SurplusAmount = additionalInterest;
                    scheduleDetails.PrincipalAmount = GetPrincipalAmountForMCA(scheduleInputDetails, lastSchdeuleDetail, payback);
                    scheduleDetails.PaymentAmount = RoundOff.Round(scheduleDetails.InterestAmount + scheduleDetails.PrincipalAmount, scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);

                    //Cumulative details
                    scheduleDetails.CumulativeInterestAmount = RoundOff.Round(lastSchdeuleDetail.CumulativeInterestAmount + scheduleDetails.InterestAmount, scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);
                    scheduleDetails.CumulativePrincipalAmount = RoundOff.Round(lastSchdeuleDetail.CumulativePrincipalAmount + scheduleDetails.PrincipalAmount, scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);
                    scheduleDetails.CumulativePaymentAmount = RoundOff.Round(lastSchdeuleDetail.CumulativePaymentAmount + scheduleDetails.PaymentAmount, scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);

                    scheduleDetails.ClosingPrincipalOutStandingBalance = RoundOff.Round(scheduleDetails.OpeningPrincipalOutStandingBalance - scheduleDetails.PrincipalAmount, scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);

                    SchduleList.Add(scheduleDetails);
                    UpdateLastScheduleInfo(lastSchdeuleDetail, scheduleDetails);
                }
            }
            if (scheduleInputDetails.InterestAdjustmentType == InterestAdjustmentType.LastInterestOnlySchedule)
                SchduleList.Add(AddLastInterestOnlyPayment(lastSchdeuleDetail, scheduleInputDetails.RemainingInterestAmount, scheduleInputDetails.PaymentFrequency, scheduleInputDetails.RoundingMethod, scheduleInputDetails.RoundingDigit));

            return new List<ILoanScheduleDetail>(SchduleList);
        }

        private LoanScheduleDetail AddFirstInterestOnlyPayment(ILastScheduleDetails lastScheduleDetail, double remainingInterest, double principal, PaymentFrequency paymentFrequency, DateTime originalStartDate, string roundingMethod, int RoundingDigit)
        {
            var lastSchedule = new LoanScheduleDetail();
            lastSchedule.Installmentnumber = lastScheduleDetail.InstallmentNumber;
            lastSchedule.InterestAmount = remainingInterest;
            lastSchedule.SurplusAmount = remainingInterest;
            lastSchedule.PrincipalAmount = 0;
            lastSchedule.PaymentAmount = remainingInterest;
            lastSchedule.CumulativeInterestAmount = RoundOff.Round(
                lastScheduleDetail.CumulativeInterestAmount + lastSchedule.InterestAmount, RoundingDigit,
                roundingMethod);
            lastSchedule.CumulativePrincipalAmount = RoundOff.Round(
                lastScheduleDetail.CumulativePrincipalAmount + lastSchedule.PrincipalAmount, RoundingDigit,
                roundingMethod);
            lastSchedule.CumulativePaymentAmount = RoundOff.Round(
                lastScheduleDetail.CumulativePaymentAmount + lastSchedule.PaymentAmount, RoundingDigit, roundingMethod);
            lastSchedule.ClosingPrincipalOutStandingBalance = principal;
            lastSchedule.OpeningPrincipalOutStandingBalance = principal;
            lastSchedule.ScheduleDate = lastScheduleDetail.LastNonHolidayScheduleDate.Date;
            lastSchedule.OriginalScheduleDate = originalStartDate.Date;
            return lastSchedule;

        }
        
        private LoanScheduleDetail AddLastInterestOnlyPayment(ILastScheduleDetails lastSchdeuleDetail, double remainingInterest, PaymentFrequency paymentFrequency, string roundingMethod, int RoundingDigit)
        {
            var lastSchedule = new LoanScheduleDetail();
            lastSchedule.Installmentnumber = lastSchdeuleDetail.InstallmentNumber;
            lastSchedule.InterestAmount = remainingInterest;
            lastSchedule.SurplusAmount = remainingInterest;
            lastSchedule.PrincipalAmount = 0;
            lastSchedule.PaymentAmount = remainingInterest;
            lastSchedule.CumulativeInterestAmount = RoundOff.Round(lastSchdeuleDetail.CumulativeInterestAmount + lastSchedule.InterestAmount, RoundingDigit, roundingMethod);
            lastSchedule.CumulativePrincipalAmount = RoundOff.Round(lastSchdeuleDetail.CumulativePrincipalAmount + lastSchedule.PrincipalAmount, RoundingDigit, roundingMethod);
            lastSchedule.CumulativePaymentAmount = RoundOff.Round(lastSchdeuleDetail.CumulativePaymentAmount + lastSchedule.PaymentAmount, RoundingDigit, roundingMethod);

            var scheduleDate = GetNextSchedule(paymentFrequency, lastSchdeuleDetail.LastNonHolidayScheduleDate);
            if (lastSchdeuleDetail.LastNonHolidayScheduleDate.Date == scheduleDate.Item1.Date)
            {
                IsStored = false;
                scheduleDate = GetNextSchedule(paymentFrequency, scheduleDate.Item1.Date);
            }
            lastSchedule.ScheduleDate = scheduleDate.Item1.Date;
            lastSchedule.OriginalScheduleDate = scheduleDate.Item2.Date;
            return lastSchedule;

        }
        
        private double GetInterestAmountForMCA(IMCAScheduleInputDetails scheduleInputDetails, LastScheduleDetails lastSchdeuleDetail, IPaybackTier payback, double FinanceCharge, double surplusAmount)
        {
            return IsLastPayment(lastSchdeuleDetail.InstallmentNumber, scheduleInputDetails.Term) ?
                RoundOff.Round((FinanceCharge - (lastSchdeuleDetail.CumulativeInterestAmount - surplusAmount)), scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod) :
                RoundOff.Round(payback.IsPercent ? FinanceCharge * (payback.FinanceCharge / payback.NoOfPayments) / 100 : (payback.FinanceCharge / payback.NoOfPayments), scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);
        }

        private double GetPrincipalAmountForMCA(IMCAScheduleInputDetails scheduleInputDetails, LastScheduleDetails lastSchdeuleDetail, IPaybackTier payback)
        {

            return IsLastPayment(lastSchdeuleDetail.InstallmentNumber, scheduleInputDetails.Term) ?
                RoundOff.Round((scheduleInputDetails.LoanAmount - lastSchdeuleDetail.CumulativePrincipalAmount), scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod) :
                RoundOff.Round(payback.IsPercent ? scheduleInputDetails.LoanAmount * (payback.Principal / payback.NoOfPayments) / 100 : (payback.Principal / payback.NoOfPayments), scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);

        }

        private static void InitializeMCAScheduleDetails(IMCAScheduleInputDetails scheduleInputDetails, LastScheduleDetails lastSchdeuleDetail)
        {
            lastSchdeuleDetail.LastPrincipalOutStandingBalance = scheduleInputDetails.LoanAmount;
            lastSchdeuleDetail.InstallmentNumber = 1;
            lastSchdeuleDetail.LastScheduleDate = scheduleInputDetails.PaymentStartDate;
            lastSchdeuleDetail.CumulativeInterestAmount = 0;
            lastSchdeuleDetail.CumulativePrincipalAmount = 0;
            lastSchdeuleDetail.CumulativePaymentAmount = 0;
        }

        private double GetInterestBasedOnAdjustType(InterestAdjustmentType interestAdjustmentType, double remainingInterest, int term, int installment)
        {
            double interestAmount = 0;
            if ((interestAdjustmentType == InterestAdjustmentType.FirstSchedule && installment == 1) || (interestAdjustmentType == InterestAdjustmentType.LastSchedule && installment == term))
                interestAmount = 0;
            else if (interestAdjustmentType == InterestAdjustmentType.DistributedSchedule)
                interestAmount = remainingInterest / term;

            return interestAmount;

        }

        #endregion

        private static void ValidateFCInput(ILoanAmortizationRequest customRequest)
        {
            if (customRequest == null)
            {
                throw new Exception("Loan Amortization Request is null");
            }

            if (customRequest.LoanAmount <= 0)
            {
                throw new Exception($"LoanAmount : expected > 0, found [{customRequest.LoanAmount}]");
            }

            if (customRequest.FactorRate <= 0)
            {
                throw new Exception($"FactorRate : expected > 0, found [{customRequest.FactorRate}]");
            }

            if (customRequest.TermInMonth <= 0)
            {
                throw new Exception($"TermInMonth : expected > 0, found [{customRequest.TermInMonth}]");
            }

            if (customRequest.SlabDetails.Count != customRequest.TermInMonth)
            {
                throw new Exception($"Mismatch : SlabDetails.Count [{customRequest.SlabDetails.Count}] does not equal " +
                    $"TermInMonth [{customRequest.TermInMonth}]");
            }
        }

        private List<ILoanScheduleDetail> GetFCSchedule(ILoanAmortizationRequest amortizationRequest)
        {
            var SchduleList = new List<LoanScheduleDetail>();

            // populates everything except InstallmentAmount
            var monthlySchedule = GetFcAmortization(amortizationRequest);

            // Now populate InstallmentAmount, based on payment frequency
            foreach (var schedule in monthlySchedule)
            {
                schedule.InstallmentAmount = getInstallmentAmount(amortizationRequest.PaymentFrequency,
                    schedule.PaymentAmount, amortizationRequest.NumberOfDaysInMonth,
                    amortizationRequest.NumberOfDaysInWeek,
                    amortizationRequest.RoundingMethod, amortizationRequest.RoundingDigit);
            }

            var tempMonthlySlab = monthlySchedule.FirstOrDefault(x => x.SequenceNumber == 1);

            var repayAmount = amortizationRequest.LoanAmount * amortizationRequest.FactorRate;

            var lastScheduleDetails = new LastScheduleDetails
            {
                InstallmentNumber = 1,
                LastScheduleDate = amortizationRequest.PaymentStartDate,
                LastPrincipalOutStandingBalance = amortizationRequest.LoanAmount,
                CumulativeInterestAmount = 0,
                CumulativePrincipalAmount = 0,
                CumulativePaymentAmount = 0
            };

            // TODO This block needs to be refactored - for later - alok
            for (var i = 0; i < amortizationRequest.Term; i++)
            {
                var scheduleDate = Tuple.Create(lastScheduleDetails.LastNonHolidayScheduleDate, lastScheduleDetails.LastNonHolidayScheduleDate, false);
                scheduleDate = lastScheduleDetails.InstallmentNumber == 1 ? scheduleDate :
                    GetNextSchedule(amortizationRequest.PaymentFrequency, lastScheduleDetails.LastNonHolidayScheduleDate);
                if (SchduleList.Any(x => x.ScheduleDate.Date == scheduleDate.Item1.Date))
                {
                    IsStored = false;
                    scheduleDate = GetNextSchedule(amortizationRequest.PaymentFrequency, scheduleDate.Item1.Date);
                }

                var scheduleDetails = new LoanScheduleDetail
                {
                    Installmentnumber = lastScheduleDetails.InstallmentNumber,
                    ScheduleDate = lastScheduleDetails.InstallmentNumber == 1 ?
                    amortizationRequest.PaymentStartDate :
                    amortizationRequest.IsHolidaySchedule ?
                    GetNextScheduleDate(amortizationRequest.PaymentFrequency, lastScheduleDetails.LastNonHolidayScheduleDate) :
                    scheduleDate.Item1,
                    PaymentAmount = RoundOff.Round(IsLastPayment(lastScheduleDetails.InstallmentNumber, amortizationRequest.Term) ?
                    repayAmount - lastScheduleDetails.CumulativePaymentAmount :
                    tempMonthlySlab.InstallmentAmount, amortizationRequest.RoundingDigit, amortizationRequest.RoundingMethod),
                    PrincipalAmount = RoundOff.Round(IsLastPayment(lastScheduleDetails.InstallmentNumber, amortizationRequest.Term) ?
                    amortizationRequest.LoanAmount - lastScheduleDetails.CumulativePrincipalAmount :
                    GetFCPrincipalAmount(amortizationRequest.PaymentFrequency, tempMonthlySlab.PrincipalAmount,
                    amortizationRequest.NumberOfDaysInMonth, amortizationRequest.NumberOfDaysInWeek),
                    amortizationRequest.RoundingDigit, amortizationRequest.RoundingMethod)
                };

                var originalScheduleDate = lastScheduleDetails.InstallmentNumber == 1 &&
                    (amortizationRequest.OriginalStartDate != null ||
                        amortizationRequest.OriginalStartDate != DateTime.MinValue) ?
                    amortizationRequest.OriginalStartDate :
                    amortizationRequest.IsHolidaySchedule ?
                    GetNextScheduleDate(amortizationRequest.PaymentFrequency, lastScheduleDetails.LastNonHolidayScheduleDate) :
                    scheduleDate.Item2;

                //daily 
                scheduleDetails.OriginalScheduleDate = amortizationRequest.PaymentFrequency == PaymentFrequency.Daily ?
                    scheduleDetails.ScheduleDate : originalScheduleDate;
                scheduleDetails.OpeningPrincipalOutStandingBalance =
                    RoundOff.Round(lastScheduleDetails.LastPrincipalOutStandingBalance,
                        amortizationRequest.RoundingDigit, amortizationRequest.RoundingMethod);

                scheduleDetails.InterestAmount = RoundOff.Round(scheduleDetails.PaymentAmount - scheduleDetails.PrincipalAmount, amortizationRequest.RoundingDigit, amortizationRequest.RoundingMethod);

                //Cumulative details
                scheduleDetails.CumulativeInterestAmount = RoundOff.Round(
                    lastScheduleDetails.CumulativeInterestAmount + scheduleDetails.InterestAmount,
                    amortizationRequest.RoundingDigit, amortizationRequest.RoundingMethod);
                scheduleDetails.CumulativePrincipalAmount = RoundOff.Round(
                    lastScheduleDetails.CumulativePrincipalAmount + scheduleDetails.PrincipalAmount,
                    amortizationRequest.RoundingDigit, amortizationRequest.RoundingMethod);
                scheduleDetails.CumulativePaymentAmount = RoundOff.Round(
                    lastScheduleDetails.CumulativePaymentAmount + scheduleDetails.PaymentAmount,
                    amortizationRequest.RoundingDigit, amortizationRequest.RoundingMethod);

                scheduleDetails.ClosingPrincipalOutStandingBalance = RoundOff.Round(
                    scheduleDetails.OpeningPrincipalOutStandingBalance - scheduleDetails.PrincipalAmount,
                    amortizationRequest.RoundingDigit, amortizationRequest.RoundingMethod);

                SchduleList.Add(scheduleDetails);
                UpdateLastScheduleInfo(lastScheduleDetails, scheduleDetails);

                //Instalmentnumber == 42? Are you kidding me?
                if ((amortizationRequest.PaymentFrequency == PaymentFrequency.Daily && scheduleDetails.Installmentnumber == 42) ||
                    (amortizationRequest.PaymentFrequency == PaymentFrequency.Weekly && scheduleDetails.Installmentnumber == 8))
                {
                    tempMonthlySlab = monthlySchedule.FirstOrDefault(x => x.SequenceNumber == 3);
                }
            }
            return new List<ILoanScheduleDetail>(SchduleList);
        }

        private static double getInstallmentAmount(PaymentFrequency frequency, double monthlyAmount, int numberOfDaysInMonth, int numberOfDaysInWeek, string roundingMethod, int roundingDigit)
        {
            switch (frequency)
            {
                case PaymentFrequency.Daily:
                    return RoundOff.Round(monthlyAmount / numberOfDaysInMonth, roundingDigit, roundingMethod);
                case PaymentFrequency.Weekly:
                    return RoundOff.Round(monthlyAmount * numberOfDaysInWeek / numberOfDaysInMonth,
                        roundingDigit, roundingMethod);
                case PaymentFrequency.BiWeekly:
                    return RoundOff.Round(monthlyAmount * numberOfDaysInWeek * 2 / numberOfDaysInMonth,
                        roundingDigit, roundingMethod);
                case PaymentFrequency.Monthly:
                case PaymentFrequency.Quarterly:
                case PaymentFrequency.SemiYearly:
                case PaymentFrequency.Yearly:
                    return monthlyAmount;
                default:
                    throw new Exception($"Invalid payment frequency [{frequency}]");
            }
        }

        private double GetFCPrincipalAmount(PaymentFrequency frequency, double monthlyPrincipalAmount, int numberOfDaysInMonth, int numberOfDaysInWeek)
        {
            var principal = monthlyPrincipalAmount;
            if (frequency == PaymentFrequency.Daily)
            {
                principal = monthlyPrincipalAmount / numberOfDaysInMonth;
            }
            else if (frequency == PaymentFrequency.Weekly)
            {
                principal = (monthlyPrincipalAmount / numberOfDaysInMonth) * numberOfDaysInWeek;
            }
            else if (frequency == PaymentFrequency.BiWeekly)
            {
                principal = (monthlyPrincipalAmount / numberOfDaysInMonth) * numberOfDaysInWeek * 2;
            }

            return principal;
        }

        private IScheduleInputDetails GetRequiredParameters(ILoanAmortizationRequest loanAmortizationRequest)
        {
            var param = new ScheduleInputDetails();
            param.PaymentFrequency = loanAmortizationRequest.PaymentFrequency;
            param.MaturityDays = 30 * loanAmortizationRequest.Term; // TODO : It should come from loanAmortizatinRequest
            param.OriginalPaymentStartDate = loanAmortizationRequest.PaymentStartDate;
            var paymentStartDate = CalendarService.GetDate(loanAmortizationRequest.PaymentStartDate.Year, loanAmortizationRequest.PaymentStartDate.Month, loanAmortizationRequest.PaymentStartDate.Day);
            param.PaymentStartDate = paymentStartDate.Type == DateType.Holiday || paymentStartDate.Type == DateType.Weekend ? paymentStartDate.NextBusinessDay.Date : loanAmortizationRequest.PaymentStartDate;
           //param.PaymentStartDate = loanAmortizationRequest.PaymentStartDate;

            param.OriginalStartDate = loanAmortizationRequest.OriginalStartDate != null && loanAmortizationRequest.OriginalStartDate != DateTime.MinValue ? loanAmortizationRequest.OriginalStartDate : loanAmortizationRequest.PaymentStartDate;
            param.RoundingDigit = loanAmortizationRequest.RoundingDigit;
            param.RoundingMethod = loanAmortizationRequest.RoundingMethod;
            param.PrincipalOutStanding = loanAmortizationRequest.LoanAmount;
            param.Term = loanAmortizationRequest.InterestAdjustmentType == InterestAdjustmentType.LastInterestOnlySchedule ? loanAmortizationRequest.Term - 1 : loanAmortizationRequest.Term;

            if (loanAmortizationRequest.PaymentStream != null)
            {
                if (loanAmortizationRequest.InterestAdjustmentType == InterestAdjustmentType.FirstInterestOnlySchedule)
                {
                    var stream = loanAmortizationRequest.PaymentStream.FirstOrDefault();
                    stream.NoOfPayments -= 1;
                }
                else if (loanAmortizationRequest.InterestAdjustmentType == InterestAdjustmentType.LastInterestOnlySchedule)
                {
                    var stream = loanAmortizationRequest.PaymentStream.LastOrDefault();
                    stream.NoOfPayments -= 1;
                }
                foreach (var stream in loanAmortizationRequest.PaymentStream)
                {
                    stream.AppliedInterestRate = InterestRate.Calculate_InterestRate(stream.PaymentFrequency.ToString(), loanAmortizationRequest.InterestRate);
                    var streamAmount = loanAmortizationRequest.LoanAmount * stream.RepayablePercent / 100;
                    stream.InstallmentAmount = loanAmortizationRequest.PaymentAmount > 0 ? loanAmortizationRequest.PaymentAmount : RoundOff.Round(PMT.Calculate_PMT(streamAmount, stream.NoOfPayments, stream.AppliedInterestRate), 2, param.RoundingMethod);
                }
            }
            var paymentStream = new List<PaymentStream>();
            if (loanAmortizationRequest.PaymentStream == null)
            {
                var term = loanAmortizationRequest.InterestHandlingType == InterestHandlingType.InterestOnly ||
                    loanAmortizationRequest.InterestAdjustmentType == InterestAdjustmentType.FirstInterestOnlySchedule ||
                    loanAmortizationRequest.InterestAdjustmentType == InterestAdjustmentType.LastInterestOnlySchedule ?
                    loanAmortizationRequest.Term - 1 : param.Term;
                var appliedRate = InterestRate.Calculate_InterestRate(loanAmortizationRequest.PaymentFrequency.ToString(), loanAmortizationRequest.InterestRate);
                var installmentAmount = loanAmortizationRequest.PaymentAmount > 0 ? loanAmortizationRequest.PaymentAmount : RoundOff.Round(PMT.Calculate_PMT(loanAmortizationRequest.LoanAmount, term, appliedRate), 2, param.RoundingMethod);
                paymentStream = new List<PaymentStream>() { new PaymentStream() { NoOfPayments = loanAmortizationRequest.InterestAdjustmentType == InterestAdjustmentType.FirstInterestOnlySchedule ? loanAmortizationRequest.Term - 1 : param.Term, PaymentFrequency = param.PaymentFrequency, RepayablePercent = 100, AppliedInterestRate = appliedRate, InstallmentAmount = installmentAmount } };
            }

            param.PaymentStream = loanAmortizationRequest.PaymentStream != null && loanAmortizationRequest.PaymentStream.Count > 0 ?
                loanAmortizationRequest.PaymentStream :
                paymentStream;

            var loanStartDate = CalendarService.GetDate(loanAmortizationRequest.LoanStartDate.Year, loanAmortizationRequest.LoanStartDate.Month, loanAmortizationRequest.LoanStartDate.Day);
            param.LoanStartDate = loanStartDate.Type == DateType.Holiday || loanStartDate.Type == DateType.Weekend ? loanStartDate.NextBusinessDay.Date : loanAmortizationRequest.LoanStartDate;
            param.OriginalLoanStartDate = loanStartDate.Date.Date;
            //param.LoanStartDate = loanAmortizationRequest.LoanStartDate;
            //param.OriginalLoanStartDate = loanAmortizationRequest.LoanStartDate;
            param.InterestHandlingType = loanAmortizationRequest.InterestHandlingType;

            param.InterestAdjustmentType = loanAmortizationRequest.InterestAdjustmentType;
            param.RemainingInterestAmount = loanAmortizationRequest.RemainingInterestAmount;
            param.ActualAmortizationType = loanAmortizationRequest.ActualAmortizationType;
            param.DailyAccrualMethod = loanAmortizationRequest.DailyAccrualMethod;
            param.MADPercentage = loanAmortizationRequest.MADPercentage;
            return param;
        }
        
        private IScheduleInputDetails GetInterestOnlyFollowedByBalloonRequiredParameter(ILoanAmortizationRequest loanAmortizationRequest)
        {
            var param = new ScheduleInputDetails();
            param.PaymentFrequency = loanAmortizationRequest.PaymentFrequency;
            var paymentStartDate = loanAmortizationRequest.PaymentStartDate.Date;
            var calendarPaymentStartDate = CalendarService.GetDate(loanAmortizationRequest.PaymentStartDate.Year,
                loanAmortizationRequest.PaymentStartDate.Month, loanAmortizationRequest.PaymentStartDate.Day);
            param.PaymentStartDate = calendarPaymentStartDate.Type == DateType.Holiday ||
                calendarPaymentStartDate.Type == DateType.Weekend ?
                calendarPaymentStartDate.NextBusinessDay.Date : loanAmortizationRequest.PaymentStartDate;
            //param.PaymentStartDate = loanAmortizationRequest.PaymentStartDate;

            param.OriginalStartDate = loanAmortizationRequest.OriginalStartDate.Date;
            param.RoundingDigit = loanAmortizationRequest.RoundingDigit;
            param.RoundingMethod = loanAmortizationRequest.RoundingMethod;
            param.PrincipalOutStanding = loanAmortizationRequest.LoanAmount;
            param.Term = loanAmortizationRequest.InterestAdjustmentType ==
                InterestAdjustmentType.LastInterestOnlySchedule ? loanAmortizationRequest.Term - 1 :
                loanAmortizationRequest.Term;

            param.LoanStartDate = loanAmortizationRequest.LoanStartDate.Date;
            param.InterestHandlingType = loanAmortizationRequest.InterestHandlingType;
            param.InterestAdjustmentType = loanAmortizationRequest.InterestAdjustmentType;
            param.RemainingInterestAmount = loanAmortizationRequest.RemainingInterestAmount;
            param.Term = loanAmortizationRequest.Term;
            param.ScheduleType = loanAmortizationRequest.ScheduleType;
            param.MADPercentage = loanAmortizationRequest.MADPercentage;
            param.NumberOfDaysInYear = loanAmortizationRequest.NumberOfDaysInYear;
            return param;
        }

        private List<ILoanScheduleDetail> GetSchedule(IScheduleInputDetails scheduleInputDetails, bool IsHolidaySchedule, string loanPortfolioType)
        {
            var SchduleList = new List<LoanScheduleDetail>();
            var lastSchdeuleDetail = new LastScheduleDetails();
            InitializeScheduleDetails(scheduleInputDetails, lastSchdeuleDetail);
            if (scheduleInputDetails.InterestAdjustmentType == InterestAdjustmentType.FirstInterestOnlySchedule)
            {
                SchduleList.Add(AddFirstInterestOnlyPayment(lastSchdeuleDetail, scheduleInputDetails.RemainingInterestAmount, lastSchdeuleDetail.LastPrincipalOutStandingBalance, scheduleInputDetails.PaymentFrequency, scheduleInputDetails.OriginalStartDate, scheduleInputDetails.RoundingMethod, scheduleInputDetails.RoundingDigit));
                UpdateLastScheduleInfo(lastSchdeuleDetail, SchduleList.FirstOrDefault());
            }
            foreach (var stream in scheduleInputDetails.PaymentStream)
            {
                for (int i = 0; i < stream.NoOfPayments; i++)
                {

                    var scheduleDetails = new LoanScheduleDetail();
                    scheduleDetails.Installmentnumber = lastSchdeuleDetail.InstallmentNumber;
                    var scheduleDate = Tuple.Create(lastSchdeuleDetail.LastNonHolidayScheduleDate, lastSchdeuleDetail.LastNonHolidayScheduleDate, false);
                    scheduleDate = lastSchdeuleDetail.InstallmentNumber == 1 ? scheduleDate : GetNextSchedule(stream.PaymentFrequency, lastSchdeuleDetail.LastNonHolidayScheduleDate);
                    if (SchduleList.Any(x => x.ScheduleDate.Date == scheduleDate.Item1.Date))
                    {
                        IsStored = false;
                        scheduleDate = GetNextSchedule(stream.PaymentFrequency, scheduleDate.Item1.Date);
                    }
                    scheduleDetails.ScheduleDate = lastSchdeuleDetail.InstallmentNumber == 1 ? scheduleInputDetails.PaymentStartDate : (IsHolidaySchedule ? GetNextScheduleDate(stream.PaymentFrequency, lastSchdeuleDetail.LastNonHolidayScheduleDate) : scheduleDate.Item1);
                    var originalScheduleDate = lastSchdeuleDetail.InstallmentNumber == 1 && (scheduleInputDetails.OriginalStartDate != null || scheduleInputDetails.OriginalStartDate != DateTime.MinValue) ? scheduleInputDetails.OriginalStartDate : (IsHolidaySchedule ? GetNextScheduleDate(stream.PaymentFrequency, lastSchdeuleDetail.LastNonHolidayScheduleDate) : scheduleDate.Item2);
                    //daily 
                    scheduleDetails.OriginalScheduleDate = stream.PaymentFrequency == PaymentFrequency.Daily ? scheduleDetails.ScheduleDate : originalScheduleDate;

                    scheduleDetails.OpeningPrincipalOutStandingBalance = RoundOff.Round(lastSchdeuleDetail.LastPrincipalOutStandingBalance, scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);

                    //payment details
                    scheduleDetails.InterestAmount = RoundOff.Round(GetInterestAmount(scheduleDetails.OpeningPrincipalOutStandingBalance, scheduleInputDetails, lastSchdeuleDetail, stream, scheduleDetails.OriginalScheduleDate), scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);
                    var additionalInterest = RoundOff.Round(GetInterestBasedOnAdjustType(scheduleInputDetails.InterestAdjustmentType, scheduleInputDetails.RemainingInterestAmount, scheduleInputDetails.Term, lastSchdeuleDetail.InstallmentNumber), scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);
                    scheduleDetails.InterestAmount = RoundOff.Round(scheduleDetails.InterestAmount + additionalInterest, scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);
                    scheduleDetails.SurplusAmount = additionalInterest;
                    scheduleDetails.PrincipalAmount = RoundOff.Round(GetPrincipalAmount(scheduleDetails.OpeningPrincipalOutStandingBalance, scheduleDetails.InterestAmount, additionalInterest, scheduleInputDetails, lastSchdeuleDetail, stream), scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);

                    if (loanPortfolioType == PortfolioType.LineOfCredit.ToString() && scheduleInputDetails.MADPercentage > 0)
                    {
                        scheduleDetails.MinimumAmountDue = RoundOff.Round((scheduleDetails.OpeningPrincipalOutStandingBalance * scheduleInputDetails.MADPercentage) / 100, scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);
                        scheduleDetails.PrincipalAmount = scheduleDetails.MinimumAmountDue;
                    }
                    scheduleDetails.PaymentAmount = RoundOff.Round(GetPaymentAmount(scheduleDetails.PrincipalAmount, scheduleDetails.InterestAmount), scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);

                    //Cumulative details
                    scheduleDetails.CumulativeInterestAmount = RoundOff.Round(lastSchdeuleDetail.CumulativeInterestAmount + scheduleDetails.InterestAmount, scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);
                    scheduleDetails.CumulativePrincipalAmount = RoundOff.Round(lastSchdeuleDetail.CumulativePrincipalAmount + scheduleDetails.PrincipalAmount, scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);
                    scheduleDetails.CumulativePaymentAmount = RoundOff.Round(lastSchdeuleDetail.CumulativePaymentAmount + scheduleDetails.PaymentAmount, scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);

                    scheduleDetails.ClosingPrincipalOutStandingBalance = RoundOff.Round(scheduleDetails.OpeningPrincipalOutStandingBalance - scheduleDetails.PrincipalAmount, scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);
                    scheduleDetails.InterestHandlingType = lastSchdeuleDetail.InstallmentNumber == 1 ? scheduleInputDetails.InterestHandlingType : InterestHandlingType.None;
                    scheduleDetails.FrequencyDays = lastSchdeuleDetail.InstallmentNumber == 1 ? GetFrequencyDays(scheduleInputDetails.LoanStartDate, scheduleInputDetails.PaymentStartDate) : GetFrequencyDays(lastSchdeuleDetail.LastNonHolidayScheduleDate, scheduleDetails.OriginalScheduleDate);

                    SchduleList.Add(scheduleDetails);
                    //update last Schedule info
                    UpdateLastScheduleInfo(lastSchdeuleDetail, scheduleDetails);

                }
            }

            if (scheduleInputDetails.InterestAdjustmentType == InterestAdjustmentType.LastInterestOnlySchedule)
                SchduleList.Add(AddLastInterestOnlyPayment(lastSchdeuleDetail, scheduleInputDetails.RemainingInterestAmount, scheduleInputDetails.PaymentFrequency, scheduleInputDetails.RoundingMethod, scheduleInputDetails.RoundingDigit));
            return new List<ILoanScheduleDetail>(SchduleList);
        }

        private Tuple<List<ILoanScheduleDetail>, List<ILoanScheduleDetail>> GetActualSchedule(IScheduleInputDetails scheduleInputDetails, bool IsHolidaySchedule, double annualRate, string loanPortfolioType)
      {
            var scheduleList = new List<ILoanScheduleDetail>();
            var dailyScheduleList = new List<ILoanScheduleDetail>();
            var lastSchdeuleDetail = new LastScheduleDetails();
            var lastSchdeuleDailyDetail = new LastScheduleDetails();

            InitializeScheduleDetails(scheduleInputDetails, lastSchdeuleDetail);
            InitializeScheduleDetails(scheduleInputDetails, lastSchdeuleDailyDetail);
            if (scheduleInputDetails.InterestAdjustmentType == InterestAdjustmentType.FirstInterestOnlySchedule)
            {
                scheduleList.Add(AddFirstInterestOnlyPayment(lastSchdeuleDetail, scheduleInputDetails.RemainingInterestAmount, lastSchdeuleDetail.LastPrincipalOutStandingBalance, scheduleInputDetails.PaymentFrequency, scheduleInputDetails.OriginalStartDate, scheduleInputDetails.RoundingMethod, scheduleInputDetails.RoundingDigit));
                UpdateLastScheduleInfo(lastSchdeuleDetail, scheduleList.FirstOrDefault());
                UpdateLastScheduleInfo(lastSchdeuleDailyDetail, scheduleList.FirstOrDefault());
            }
            foreach (var stream in scheduleInputDetails.PaymentStream)
            {
                var installmentAmount = stream.InstallmentAmount;
                double dailyInterestRate = 0;
                bool isLast = false;
                for (int i = 0; i < stream.NoOfPayments; i++)
                {
                    isLast = i == stream.NoOfPayments - 1;
                    var scheduleDetails = new LoanScheduleDetail();
                    scheduleDetails.Installmentnumber = lastSchdeuleDetail.InstallmentNumber;
                    var scheduleDate = Tuple.Create(lastSchdeuleDetail.LastNonHolidayScheduleDate, lastSchdeuleDetail.LastNonHolidayScheduleDate, false);
                    scheduleDate = lastSchdeuleDetail.InstallmentNumber == 1 ? scheduleDate : GetNextSchedule(stream.PaymentFrequency, lastSchdeuleDetail.LastNonHolidayScheduleDate);
                    if (scheduleList.Any(x => x.ScheduleDate.Date == scheduleDate.Item1.Date))
                    {
                        IsStored = false;
                        scheduleDate = GetNextSchedule(stream.PaymentFrequency, scheduleDate.Item1.Date);
                    }
                    scheduleDetails.ScheduleDate = lastSchdeuleDetail.InstallmentNumber == 1 ? scheduleInputDetails.PaymentStartDate : (IsHolidaySchedule ? GetNextScheduleDate(stream.PaymentFrequency, lastSchdeuleDetail.LastNonHolidayScheduleDate) : scheduleDate.Item1);
                    var originalScheduleDate = lastSchdeuleDetail.InstallmentNumber == 1 && (scheduleInputDetails.OriginalStartDate != null || scheduleInputDetails.OriginalStartDate != DateTime.MinValue) ? scheduleInputDetails.OriginalStartDate : (IsHolidaySchedule ? GetNextScheduleDate(stream.PaymentFrequency, lastSchdeuleDetail.LastNonHolidayScheduleDate) : scheduleDate.Item2);
                    scheduleDetails.OriginalScheduleDate = stream.PaymentFrequency == PaymentFrequency.Daily ? scheduleDetails.ScheduleDate : originalScheduleDate;
                    var previousScheduleDate = Helper.GetFirstPaymentDateBasedOnFrequency(stream.PaymentFrequency.ToString(),scheduleDetails.OriginalScheduleDate);
                    scheduleDetails.FrequencyDays = lastSchdeuleDetail.InstallmentNumber == 1 ? GetFrequencyDays(scheduleInputDetails.InterestAdjustmentType == InterestAdjustmentType.None ? previousScheduleDate.Value : scheduleInputDetails.OriginalLoanStartDate, scheduleDetails.OriginalScheduleDate) : GetFrequencyDays(lastSchdeuleDetail.LastNonHolidayScheduleDate, scheduleDetails.OriginalScheduleDate);
                    scheduleDetails.OpeningPrincipalOutStandingBalance = RoundOff.Round(lastSchdeuleDetail.LastPrincipalOutStandingBalance, scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);
                    scheduleList.Add(scheduleDetails);

                    var startDate = lastSchdeuleDetail.InstallmentNumber == 1 && scheduleInputDetails.InterestAdjustmentType != InterestAdjustmentType.FirstInterestOnlySchedule ? scheduleInputDetails.OriginalLoanStartDate : lastSchdeuleDetail.LastNonHolidayScheduleDate;
                    var endDate = scheduleDetails.OriginalScheduleDate;
                    

                    for (DateTime j = startDate; j < endDate; j = j.AddDays(1))
                    {
                        if (scheduleInputDetails.InterestAdjustmentType == InterestAdjustmentType.None)
                        {
                            if (j<previousScheduleDate)
                            {
                                continue;
                            }
                        }
                        var days = Helper.GetDaysInYearForProvidedDate(j, endDate);
                        dailyInterestRate = InterestRate.Calculate_InterestRate("daily", annualRate, Convert.ToInt32(days));

                        var dailyScheduleDetails = new LoanScheduleDetail();
                        dailyScheduleDetails.OpeningPrincipalOutStandingBalance = scheduleDetails.OpeningPrincipalOutStandingBalance;

                        dailyScheduleDetails.Installmentnumber = scheduleDetails.Installmentnumber;
                        dailyScheduleDetails.InterestAmount = dailyInterestRate * 1 * dailyScheduleDetails.OpeningPrincipalOutStandingBalance / 100;

                        stream.InstallmentAmount = (installmentAmount / scheduleDetails.FrequencyDays);
                        if (isLast == true && j == endDate.AddDays(-1))
                        {
                            dailyScheduleDetails.PrincipalAmount = RoundOff.Round(GetPrincipalAmount(dailyScheduleDetails.OpeningPrincipalOutStandingBalance, dailyScheduleDetails.InterestAmount, 0, scheduleInputDetails, lastSchdeuleDetail, stream), scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);

                        }
                        else
                        {
                            dailyScheduleDetails.PrincipalAmount = GetPrincipalAmount(dailyScheduleDetails.OpeningPrincipalOutStandingBalance, dailyScheduleDetails.InterestAmount, 0, scheduleInputDetails, lastSchdeuleDetail, stream, true);
                        }
                        dailyScheduleDetails.PaymentAmount = GetPaymentAmount(dailyScheduleDetails.PrincipalAmount, dailyScheduleDetails.InterestAmount);
                        dailyScheduleDetails.ScheduleDate = scheduleDetails.ScheduleDate;
                        dailyScheduleDetails.OriginalScheduleDate = scheduleDetails.OriginalScheduleDate;
                        dailyScheduleDetails.ClosingPrincipalOutStandingBalance = RoundOff.Round(dailyScheduleDetails.OpeningPrincipalOutStandingBalance - dailyScheduleDetails.PrincipalAmount, scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);
                        dailyScheduleList.Add(dailyScheduleDetails);
                        UpdateLastScheduleInfo(lastSchdeuleDailyDetail, dailyScheduleDetails);

                    }
                    scheduleDetails.InterestAmount = RoundOff.Round(dailyScheduleList.Where(x => x.Installmentnumber == scheduleDetails.Installmentnumber).Sum(x => x.InterestAmount), scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);
                    var additionalInterest = RoundOff.Round(GetInterestBasedOnAdjustType(scheduleInputDetails.InterestAdjustmentType, scheduleInputDetails.RemainingInterestAmount, scheduleInputDetails.Term, lastSchdeuleDetail.InstallmentNumber), scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);
                    scheduleDetails.InterestAmount = scheduleDetails.InterestAmount + additionalInterest;

                    if(isLast)
                    {
                        scheduleDetails.PrincipalAmount =  RoundOff.Round(lastSchdeuleDetail.LastPrincipalOutStandingBalance, scheduleInputDetails.RoundingDigit,scheduleInputDetails.RoundingMethod);
                        scheduleDetails.PaymentAmount =  RoundOff.Round(scheduleDetails.PrincipalAmount + scheduleDetails.InterestAmount, scheduleInputDetails.RoundingDigit,scheduleInputDetails.RoundingMethod);

                    }
                    else
                    {
                        scheduleDetails.PrincipalAmount = RoundOff.Round(dailyScheduleList.Where(x => x.Installmentnumber ==    scheduleDetails.Installmentnumber).Sum(x => x.PrincipalAmount), scheduleInputDetails.RoundingDigit,    scheduleInputDetails.RoundingMethod);
                        scheduleDetails.PaymentAmount = RoundOff.Round(dailyScheduleList.Where(x => x.Installmentnumber ==  scheduleDetails.Installmentnumber).Sum(x => x.PaymentAmount), scheduleInputDetails.RoundingDigit,    scheduleInputDetails.RoundingMethod);
                    }

                    if (loanPortfolioType == PortfolioType.LineOfCredit.ToString() && scheduleInputDetails.MADPercentage > 0)
                    {
                        scheduleDetails.MinimumAmountDue = RoundOff.Round((scheduleDetails.OpeningPrincipalOutStandingBalance * scheduleInputDetails.MADPercentage) / 100, scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);
                        scheduleDetails.PrincipalAmount = scheduleDetails.MinimumAmountDue;
                        scheduleDetails.PaymentAmount = RoundOff.Round(GetPaymentAmount(scheduleDetails.PrincipalAmount, scheduleDetails.InterestAmount), scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);
                    }

                    scheduleDetails.CumulativeInterestAmount = RoundOff.Round(lastSchdeuleDetail.CumulativeInterestAmount + scheduleDetails.InterestAmount, scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);
                    scheduleDetails.CumulativePrincipalAmount = RoundOff.Round(lastSchdeuleDetail.CumulativePrincipalAmount + scheduleDetails.PrincipalAmount, scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);
                    scheduleDetails.CumulativePaymentAmount = RoundOff.Round(lastSchdeuleDetail.CumulativePaymentAmount + scheduleDetails.PaymentAmount, scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);
                    scheduleDetails.InterestHandlingType = lastSchdeuleDetail.InstallmentNumber == 1 ? scheduleInputDetails.InterestHandlingType : InterestHandlingType.None;

                    scheduleDetails.ClosingPrincipalOutStandingBalance = scheduleDetails.OpeningPrincipalOutStandingBalance - scheduleDetails.PrincipalAmount;
                    UpdateLastScheduleInfo(lastSchdeuleDetail, scheduleDetails);
                }
            }

            if (scheduleInputDetails.InterestAdjustmentType == InterestAdjustmentType.LastInterestOnlySchedule)
            {
                scheduleList.Add(AddLastInterestOnlyPayment(lastSchdeuleDetail, scheduleInputDetails.RemainingInterestAmount, scheduleInputDetails.PaymentFrequency, scheduleInputDetails.RoundingMethod, scheduleInputDetails.RoundingDigit));
            }
            return Tuple.Create(scheduleList, dailyScheduleList);
        }

        private Tuple<List<ILoanScheduleDetail>, List<ILoanScheduleDetail>> GetInterestOnlyFollowedByBalloonActualSchedule(IScheduleInputDetails scheduleInputDetails, double annualRate, string loanPortfolioType)
        {

            if (loanPortfolioType == PortfolioType.SCF.ToString())
            {
                return GetInterestOnlyFollowedByBalloonActualScheduleForSCF(scheduleInputDetails, annualRate);
            }
            else
            {
                return GetInterestOnlyFollowedByBalloonActualScheduleForNonSCF(scheduleInputDetails, annualRate);
            }
        }

        private double GetInterestAmount(double outStandingAmount, IScheduleInputDetails scheduleInputDetails, LastScheduleDetails lastScheduleDetail, PaymentStream stream, DateTime nextPaymentDate)
        {
            var daysInMonth = 30;
            if (scheduleInputDetails.DailyAccrualMethod != "30/360")
            {
                daysInMonth = DateTime.DaysInMonth(scheduleInputDetails.OriginalLoanStartDate.Date.Year, scheduleInputDetails.OriginalLoanStartDate.Date.Month);
            }
            if (lastScheduleDetail.InstallmentNumber == 1 && (scheduleInputDetails.InterestHandlingType == InterestHandlingType.PartialInterest || scheduleInputDetails.InterestHandlingType == InterestHandlingType.InterestOnly))
            {
                var dailyRate = (scheduleInputDetails.PaymentFrequency == PaymentFrequency.Monthly ?
                    (stream.AppliedInterestRate / daysInMonth) :
                    scheduleInputDetails.PaymentFrequency == PaymentFrequency.Weekly ?
                    (stream.AppliedInterestRate / 7) :
                    scheduleInputDetails.PaymentFrequency == PaymentFrequency.BiWeekly ?
                    (stream.AppliedInterestRate / 14) :
                    stream.AppliedInterestRate);
                var days = 0;
                var startDate = scheduleInputDetails.OriginalLoanStartDate;
                // to calculate number of days accrued
                for (int i = 0; startDate < scheduleInputDetails.OriginalStartDate.Date; i++)
                {
                    days = days + GetAccrualdays(startDate, stream.PaymentFrequency, scheduleInputDetails.DailyAccrualMethod);
                    startDate = startDate.AddDays(1);
                }

                return (dailyRate * days * outStandingAmount) / 100;
            }
            else if (lastScheduleDetail.InstallmentNumber == 2 && scheduleInputDetails.InterestHandlingType == InterestHandlingType.PartialInterest && scheduleInputDetails.InterestAdjustmentType == InterestAdjustmentType.FirstInterestOnlySchedule)
            {
                var dailyRate = (scheduleInputDetails.PaymentFrequency == PaymentFrequency.Monthly ?
                    (stream.AppliedInterestRate / daysInMonth) :
                    scheduleInputDetails.PaymentFrequency == PaymentFrequency.Weekly ?
                    (stream.AppliedInterestRate / 7) :
                    scheduleInputDetails.PaymentFrequency == PaymentFrequency.BiWeekly ?
                    (stream.AppliedInterestRate / 14) :
                    stream.AppliedInterestRate);
                var days = 0;
                var startDate = scheduleInputDetails.OriginalLoanStartDate;
                // to calculate number of days accrued
                for (int i = 0; startDate < nextPaymentDate.Date; i++)
                {
                    days = days + GetAccrualdays(startDate, stream.PaymentFrequency, scheduleInputDetails.DailyAccrualMethod);
                    startDate = startDate.AddDays(1);
                }

                return (dailyRate * days * outStandingAmount) / 100;
            }
            else if (lastScheduleDetail.InstallmentNumber == 1 && (scheduleInputDetails.InterestAdjustmentType == InterestAdjustmentType.FirstSchedule || scheduleInputDetails.InterestAdjustmentType == InterestAdjustmentType.LastSchedule))
            {
                var days = 0;
                var startDate = scheduleInputDetails.OriginalLoanStartDate;
                var nextScheduleDate = scheduleInputDetails.OriginalStartDate;
                var interestAmount = 0.0;
                if ((scheduleInputDetails.OriginalStartDate.Date - scheduleInputDetails.OriginalLoanStartDate.Date).Days > daysInMonth)
                {
                    nextScheduleDate = Helper.GetFirstPaymentDateBasedOnFrequency(scheduleInputDetails.PaymentFrequency.ToString(), scheduleInputDetails.OriginalStartDate).Value;
                }
                var previousCalculateDate = Helper.GetFirstPaymentDateBasedOnFrequency(scheduleInputDetails.PaymentFrequency.ToString(), nextScheduleDate);
                var dailyRate = (scheduleInputDetails.PaymentFrequency == PaymentFrequency.Monthly ?
                    (stream.AppliedInterestRate / (nextScheduleDate - previousCalculateDate.Value).TotalDays) :
                    scheduleInputDetails.PaymentFrequency == PaymentFrequency.Weekly ?
                    (stream.AppliedInterestRate / 7) :
                    scheduleInputDetails.PaymentFrequency == PaymentFrequency.BiWeekly ?
                    (stream.AppliedInterestRate / 14) :
                    stream.AppliedInterestRate);
                // to calculate number of days accrued
                for (int i = 0; startDate < scheduleInputDetails.OriginalStartDate.Date; i++)
                {
                    days = days + GetAccrualdays(startDate, stream.PaymentFrequency, scheduleInputDetails.DailyAccrualMethod);
                    startDate = startDate.AddDays(1);
                    if (nextScheduleDate == startDate)
                    {
                        interestAmount += (dailyRate * days * outStandingAmount) / 100;
                        days = 0;
                        dailyRate = (scheduleInputDetails.PaymentFrequency == PaymentFrequency.Monthly ?
                            (stream.AppliedInterestRate / DateTime.DaysInMonth(nextScheduleDate.Date.Year, nextScheduleDate.Date.Month)) :
                            scheduleInputDetails.PaymentFrequency == PaymentFrequency.Weekly ?
                            (stream.AppliedInterestRate / 7) :
                            scheduleInputDetails.PaymentFrequency == PaymentFrequency.BiWeekly ?
                            (stream.AppliedInterestRate / 14) :
                            stream.AppliedInterestRate);
                    }
                }

                return interestAmount + (dailyRate * days * outStandingAmount) / 100;
            }
            else
                return stream.AppliedInterestRate * outStandingAmount / 100;
        }
        
        private int GetAccrualdays(DateTime processingDate, PaymentFrequency loanFrequency, string dailyAccrualMethod)
        {
            if (loanFrequency == PaymentFrequency.Monthly && dailyAccrualMethod == "30/360")
            {
                bool isLeapYear = false;
                if ((processingDate.Year % 4 == 0 && processingDate.Year % 100 != 0) || (processingDate.Year % 400 == 0))
                    isLeapYear = true;

                if (isLeapYear == true && processingDate.Month == 2 && processingDate.Day == 29)
                    return 2;
                if (isLeapYear == false && processingDate.Month == 2 && processingDate.Day == 28)
                    return 3;
                else if (processingDate.Day == 31)
                    return 0;
                else
                    return 1;
            }
            else
                return 1;
        }

        private double GetPrincipalAmount(double outStandingAmount, double interest, double additionalInterest, IScheduleInputDetails scheduleInputDetails, LastScheduleDetails lastScheduleDetail, PaymentStream stream, bool isLastOverride = false)
        {
            double principalAmount = 0;
            if ((scheduleInputDetails.InterestHandlingType == InterestHandlingType.InterestOnly || scheduleInputDetails.InterestAdjustmentType == InterestAdjustmentType.FirstInterestOnlySchedule) && lastScheduleDetail.InstallmentNumber == 1)
                return 0;
            else if (IsLastPayment(lastScheduleDetail.InstallmentNumber, scheduleInputDetails.Term) && isLastOverride == false)
            {
                principalAmount = outStandingAmount;
                //catching all principal outstanding as principal amount to be recovered for last schedule.
            }
            else if (scheduleInputDetails.InterestAdjustmentType == InterestAdjustmentType.FirstSchedule)
            {
                principalAmount = stream.InstallmentAmount - interest;
                //For handling first emi of broken period where broken period interest will merged with first schedule.
            }
            else
                principalAmount = (stream.InstallmentAmount + additionalInterest) - interest;
            return principalAmount;
        }

        private double GetPaymentAmount(double principal, double interest)
        {
            double paymentAmount = 0;
            paymentAmount = principal + interest;
            return paymentAmount;
        }

        private bool IsLastPayment(int installmentNumber, int term)
        {
            if (installmentNumber == term)
                return true;
            else
                return false;

        }

        private static void InitializeScheduleDetails(IScheduleInputDetails scheduleInputDetails, LastScheduleDetails lastSchdeuleDetail)
        {
            lastSchdeuleDetail.LastPrincipalOutStandingBalance = scheduleInputDetails.PrincipalOutStanding;
            lastSchdeuleDetail.InstallmentNumber = 1;
            lastSchdeuleDetail.LastScheduleDate = scheduleInputDetails.OriginalPaymentStartDate;
            lastSchdeuleDetail.LastNonHolidayScheduleDate = scheduleInputDetails.PaymentStartDate;
            lastSchdeuleDetail.CumulativeInterestAmount = 0;
            lastSchdeuleDetail.CumulativePrincipalAmount = 0;
            lastSchdeuleDetail.CumulativePaymentAmount = 0;
        }
        
        private Tuple<DateTime, DateTime, bool> GetNextSchedule(PaymentFrequency paymentFrequency, DateTime lastSchduleDate, bool? IsDoubleOnHoliday = false)
        {
            if (IsStored)
            {
                lastSchduleDate = StoredDate;
            }
            var nextScheduleDate = GetNextScheduleDate(paymentFrequency, lastSchduleDate);
            StoredDate = nextScheduleDate;
            IsStored = false;
            bool IsHoliday = false;
            var todayInfo = CalendarService.GetDate(nextScheduleDate.Year, nextScheduleDate.Month, nextScheduleDate.Day);
            if (todayInfo.Type == DateType.Holiday || todayInfo.Type == DateType.Weekend)
            {
                IsStored = true;
                nextScheduleDate = todayInfo.NextBusinessDay.Date;
                if (IsDoubleOnHoliday.Value == true)
                {
                    var term = (todayInfo.NextBusinessDay.Date - todayInfo.Date).Days + 1;
                    var holidayDate = CalendarService.GetPeriodicDates(todayInfo.Date.Date, term, Periodicity.Daily);

                    foreach (var item in holidayDate)
                    {
                        if (item.Type == DateType.Holiday)
                        {
                            IsHoliday = true;
                            StoredDate = item.Date.Date;
                            break;
                        }
                    }
                }
            }
            return Tuple.Create(nextScheduleDate, StoredDate, IsHoliday);

        }

        private int GetFrequencyDays(DateTime startDate, DateTime endDate)
        {
            return Convert.ToInt16((endDate - startDate).TotalDays);
        }
        
        private DateTime GetNextScheduleDate(PaymentFrequency paymentFrequency, DateTime lastSchduleDate)
        {
            var nextSchdeuleDate = lastSchduleDate;
            switch (paymentFrequency)
            {
                case PaymentFrequency.Daily:
                    nextSchdeuleDate = lastSchduleDate.AddDays(1);
                    break;
                case PaymentFrequency.Weekly:
                    nextSchdeuleDate = lastSchduleDate.AddDays(Period_Constants.Weekly_Period_per_days);
                    break;
                case PaymentFrequency.BiWeekly:
                    nextSchdeuleDate = lastSchduleDate.AddDays(Period_Constants.BiWeekly_Period_per_days);
                    break;
                case PaymentFrequency.Monthly:
                    nextSchdeuleDate = lastSchduleDate.AddMonths(1);
                    break;
                case PaymentFrequency.Quarterly:
                    nextSchdeuleDate = lastSchduleDate.AddMonths(Period_Constants.Quarterly_Period_per_year);
                    break;
                case PaymentFrequency.SemiYearly:
                    nextSchdeuleDate = lastSchduleDate.AddMonths(Period_Constants.Semi_Yearly_Period_per_year);
                    break;
                case PaymentFrequency.Yearly:
                    nextSchdeuleDate = lastSchduleDate.AddYears(1);
                    break;
                default:
                    throw new InvalidOperationException("Invalid Payment Frequency");
            }
            return nextSchdeuleDate;

        }

        private static void UpdateLastScheduleInfo(ILastScheduleDetails lastSchdeuleDetail, ILoanScheduleDetail scheduleDetails)
        {
            lastSchdeuleDetail.CumulativePrincipalAmount = scheduleDetails.ClosingPrincipalOutStandingBalance;
            lastSchdeuleDetail.CumulativeInterestAmount = scheduleDetails.CumulativeInterestAmount;
            lastSchdeuleDetail.CumulativePrincipalAmount = scheduleDetails.CumulativePrincipalAmount;
            lastSchdeuleDetail.CumulativePaymentAmount = scheduleDetails.CumulativePaymentAmount;
            lastSchdeuleDetail.LastPrincipalOutStandingBalance = scheduleDetails.ClosingPrincipalOutStandingBalance;
            lastSchdeuleDetail.LastScheduleDate = scheduleDetails.OriginalScheduleDate;
            lastSchdeuleDetail.LastNonHolidayScheduleDate = scheduleDetails.ScheduleDate;
            lastSchdeuleDetail.InstallmentNumber++;
        }

        private static void ValidateRequest(ILoanAmortizationRequest loanAmortizationRequest)
        {
            if (loanAmortizationRequest == null)
                throw new ArgumentException($"#{nameof(loanAmortizationRequest)} cannot be null", nameof(loanAmortizationRequest));

            if (string.IsNullOrWhiteSpace(loanAmortizationRequest.PortfolioType))
                throw new ArgumentException($"#{nameof(loanAmortizationRequest.PortfolioType)} cannot be null", nameof(loanAmortizationRequest.PortfolioType));

            if (loanAmortizationRequest.LoanAmount <= 0)
                throw new ArgumentException($"#{nameof(loanAmortizationRequest.LoanAmount)} should be grater than zero");

            if (!Enum.IsDefined(typeof(PaymentFrequency), loanAmortizationRequest.PaymentFrequency))
                throw new ArgumentException($"#{nameof(loanAmortizationRequest.PaymentFrequency)} cannot be null", nameof(loanAmortizationRequest.PaymentFrequency));

        }

        private static PortfolioType GetPortfolioType(string p)
        {
            foreach (PortfolioType validValue in Enum.GetValues(typeof(PortfolioType)))
            {
                if (validValue.ToString().ToLower().Equals(p.ToLower()))
                {
                    return validValue;
                }
            }
            throw new Exception($"Unable to convert {p} to a valid PortfolioType.");
        }

        private static bool isFCScenario(List<SlabDetails> s)
        {
            return s != null && s.Count > 0 && 100 == s.Sum(x => x.Percentage);
        }

        private Tuple<List<ILoanScheduleDetail>, List<ILoanScheduleDetail>> GetInterestOnlyFollowedByBalloonActualScheduleForSCF(IScheduleInputDetails scheduleInputDetails, double annualRate)
        {
            var scheduleList = new List<ILoanScheduleDetail>();
            var dailyScheduleList = new List<ILoanScheduleDetail>();
            var firstBillingDate = scheduleInputDetails.OriginalStartDate.Date;
            var onBoardingDate = scheduleInputDetails.LoanStartDate.Date;
            var amortStartDate = scheduleInputDetails.LoanStartDate.Date;

            var nextBillingDate = firstBillingDate;
            var loanAmount = scheduleInputDetails.PrincipalOutStanding;
            var firstPaymentLastPayment = false;
            var maturityDays = scheduleInputDetails.Term * 30;

            var loanOriginalEndDate = onBoardingDate.AddDays(maturityDays);
            var loanActualEndDate = loanOriginalEndDate;

            var loanEndDateInfo = CalendarService.GetDate(loanOriginalEndDate.Year, loanOriginalEndDate.Month,
                loanOriginalEndDate.Day);

            if (loanEndDateInfo.Type == DateType.Holiday || loanEndDateInfo.Type == DateType.Weekend)
            {
                loanActualEndDate = loanEndDateInfo.NextBusinessDay.Date;
            }

            for (var i = 0; amortStartDate.Date < loanOriginalEndDate.Date; i++)
            {
                var isLastPayment = false;
                if (amortStartDate.Date == onBoardingDate.Date)
                {
                    var scheduleDetails = new LoanScheduleDetail();
                    scheduleDetails.Installmentnumber = 1;
                    scheduleDetails.InterestHandlingType = InterestHandlingType.InterestOnly;
                    scheduleDetails.OpeningPrincipalOutStandingBalance = loanAmount;
                    scheduleDetails.OriginalScheduleDate = firstBillingDate > loanOriginalEndDate ? loanOriginalEndDate : firstBillingDate;

                    if (firstBillingDate.Date != scheduleInputDetails.PaymentStartDate.Date)
                    {
                        scheduleDetails.ScheduleDate = firstBillingDate.Date > loanOriginalEndDate ? loanOriginalEndDate : scheduleInputDetails.PaymentStartDate.Date;
                    }
                    else
                    {
                        scheduleDetails.ScheduleDate = firstBillingDate > loanOriginalEndDate ? loanOriginalEndDate : firstBillingDate;
                    }

                    if (firstBillingDate.Date != scheduleInputDetails.PaymentStartDate.Date)
                    {
                        if (firstBillingDate.Date > loanOriginalEndDate)
                        {
                            if (loanOriginalEndDate.Date == loanActualEndDate.Date)
                            {
                                scheduleDetails.ScheduleDate = loanOriginalEndDate.Date;
                            }
                            else
                            {
                                scheduleDetails.ScheduleDate = loanActualEndDate.Date;
                            }
                        }
                        else
                        {
                            scheduleDetails.ScheduleDate = scheduleInputDetails.PaymentStartDate.Date;
                        }
                    }
                    else
                    {
                        scheduleDetails.ScheduleDate = firstBillingDate > loanOriginalEndDate ?
                            loanOriginalEndDate :
                            firstBillingDate;
                    }

                    scheduleDetails.PrincipalAmount = 0;
                    scheduleDetails.ClosingPrincipalOutStandingBalance = loanAmount;

                    if (firstBillingDate > loanOriginalEndDate)
                    {
                        isLastPayment = true;
                        firstPaymentLastPayment = true;
                        firstBillingDate = loanOriginalEndDate;
                    }

                    for (DateTime j = amortStartDate.Date; j < firstBillingDate; j = j.AddDays(1))
                    {
                        // For SCF, We will have number of days in a year as input from product parameter. 
                        // As mentioned in https://projects.codebase.guru/browse/LFPROJ-1151 ticket.
                        //var days = Helper.GetDaysInYearForProvidedDate(j, firstBillingDate);
                        var dailyInterestRate = InterestRate.Calculate_InterestRate("daily", annualRate, scheduleInputDetails.NumberOfDaysInYear);

                        var dailyScheduleDetails = new LoanScheduleDetail();
                        dailyScheduleDetails.Installmentnumber = scheduleDetails.Installmentnumber;
                        dailyScheduleDetails.InterestAmount = dailyInterestRate * 1 * loanAmount / 100;
                        dailyScheduleDetails.CumulativeInterestAmount =
                            dailyScheduleDetails.InterestAmount + (dailyScheduleList.Any() ?
                                dailyScheduleList.OrderByDescending(l => l.ScheduleDate)
                                .Select(l => l.CumulativeInterestAmount).FirstOrDefault() : 0);
                        dailyScheduleDetails.PrincipalAmount = 0;
                        dailyScheduleDetails.PaymentAmount = 0;
                        dailyScheduleDetails.ScheduleDate = j;
                        dailyScheduleDetails.OriginalScheduleDate = j;
                        dailyScheduleList.Add(dailyScheduleDetails);
                    }
                    scheduleDetails.InterestAmount = RoundOff.Round(dailyScheduleList
                        .Where(x => x.Installmentnumber == scheduleDetails.Installmentnumber)
                        .Sum(x => x.InterestAmount) + scheduleInputDetails.RemainingInterestAmount,
                        scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);

                    scheduleDetails.PrincipalAmount = isLastPayment ? loanAmount : 0;
                    scheduleDetails.PaymentAmount = RoundOff.Round(
                        dailyScheduleList.Where(x => x.Installmentnumber == scheduleDetails.Installmentnumber)
                        .Sum(x => x.InterestAmount) + scheduleDetails.PrincipalAmount  + scheduleInputDetails.RemainingInterestAmount,
                        scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);

                    scheduleDetails.CumulativeInterestAmount = scheduleDetails.InterestAmount;
                    scheduleDetails.CumulativePrincipalAmount = RoundOff.Round(isLastPayment ? loanAmount : 0,
                        scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);
                    scheduleDetails.CumulativePaymentAmount = RoundOff.Round(scheduleDetails.CumulativeInterestAmount + scheduleDetails.CumulativePrincipalAmount,
                        scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);
                    scheduleDetails.InterestHandlingType = scheduleInputDetails.InterestHandlingType;
                    scheduleDetails.SurplusAmount = scheduleInputDetails.RemainingInterestAmount;
                    scheduleDetails.ClosingPrincipalOutStandingBalance = scheduleDetails.OpeningPrincipalOutStandingBalance - scheduleDetails.PrincipalAmount;
                    scheduleList.Add(scheduleDetails);
                    amortStartDate = firstBillingDate.Date;
                    continue;
                }

                nextBillingDate = GetNextScheduleDate(scheduleInputDetails.PaymentFrequency, nextBillingDate);
                if (nextBillingDate >= loanOriginalEndDate.Date)
                {
                    if (firstPaymentLastPayment == true)
                    {
                        return Tuple.Create(scheduleList, dailyScheduleList);
                    }
                    nextBillingDate = loanOriginalEndDate;
                    isLastPayment = true;

                }
                var scheduleDetailOthers = new LoanScheduleDetail();

                scheduleDetailOthers.Installmentnumber = scheduleList.OrderByDescending(k => k.ScheduleDate)
                    .FirstOrDefault().Installmentnumber + 1;
                scheduleDetailOthers.InterestHandlingType = isLastPayment ? InterestHandlingType.InterestPrincipal :
                    InterestHandlingType.InterestOnly;
                scheduleDetailOthers.OpeningPrincipalOutStandingBalance = loanAmount;
                scheduleDetailOthers.ScheduleDate = nextBillingDate;
                scheduleDetailOthers.PrincipalAmount = isLastPayment ? loanAmount : 0;
                scheduleDetailOthers.ClosingPrincipalOutStandingBalance = isLastPayment ? 0 : loanAmount;
                scheduleDetailOthers.OriginalScheduleDate = nextBillingDate;
                var nextBillingDateFrom = CalendarService.GetDate(
                    nextBillingDate.Year, nextBillingDate.Month, nextBillingDate.Day);

                if (nextBillingDateFrom.Type == DateType.Holiday || nextBillingDateFrom.Type == DateType.Weekend)
                {
                    scheduleDetailOthers.ScheduleDate = nextBillingDateFrom.NextBusinessDay.Date;
                }

                for (DateTime j = amortStartDate.Date; j < scheduleDetailOthers.OriginalScheduleDate; j = j.AddDays(1))
                {
                    // For SCF, We will have number of days in a year as input from product parameter. 
                    // As mentioned in https://projects.codebase.guru/browse/LFPROJ-1151 ticket.
                    //var days = Helper.GetDaysInYearForProvidedDate(j, scheduleDetailOthers.OriginalScheduleDate);
                    var dailyInterestRate = InterestRate.Calculate_InterestRate("daily", annualRate, scheduleInputDetails.NumberOfDaysInYear);

                    var dailyScheduleDetails = new LoanScheduleDetail();
                    dailyScheduleDetails.Installmentnumber = scheduleDetailOthers.Installmentnumber;
                    dailyScheduleDetails.InterestAmount = dailyInterestRate * 1 * loanAmount / 100;
                    dailyScheduleDetails.CumulativeInterestAmount =
                        dailyScheduleDetails.InterestAmount + (dailyScheduleList.Any() ?
                            dailyScheduleList.OrderByDescending(l => l.ScheduleDate)
                            .Select(l => l.CumulativeInterestAmount).FirstOrDefault() : 0);
                    dailyScheduleDetails.PrincipalAmount = 0;
                    dailyScheduleDetails.PaymentAmount = 0;
                    dailyScheduleDetails.ScheduleDate = j;
                    dailyScheduleDetails.OriginalScheduleDate = j;
                    dailyScheduleList.Add(dailyScheduleDetails);
                }
                scheduleDetailOthers.InterestAmount = RoundOff.Round(
                    dailyScheduleList.Where(x => x.Installmentnumber == scheduleDetailOthers.Installmentnumber)
                    .Sum(x => x.InterestAmount),
                    scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);

                scheduleDetailOthers.InterestAmount = scheduleDetailOthers.InterestAmount;

                scheduleDetailOthers.PaymentAmount = RoundOff.Round(
                    dailyScheduleList.Where(x => x.Installmentnumber == scheduleDetailOthers.Installmentnumber)
                    .Sum(x => x.InterestAmount) + scheduleDetailOthers.PrincipalAmount,
                    scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);

                scheduleDetailOthers.CumulativeInterestAmount = RoundOff.Round(dailyScheduleList
                    .Sum(x => x.InterestAmount), scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);
                scheduleDetailOthers.CumulativePrincipalAmount = isLastPayment == true ? loanAmount : 0;
                scheduleDetailOthers.CumulativePaymentAmount = RoundOff.Round(
                    scheduleDetailOthers.CumulativeInterestAmount + scheduleDetailOthers.CumulativePrincipalAmount,
                    scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);
                scheduleDetailOthers.InterestHandlingType = scheduleInputDetails.InterestHandlingType;

                scheduleDetailOthers.ClosingPrincipalOutStandingBalance =
                    scheduleDetailOthers.OpeningPrincipalOutStandingBalance - scheduleDetailOthers.PrincipalAmount;
                scheduleList.Add(scheduleDetailOthers);
                amortStartDate = nextBillingDate;

            }
            return Tuple.Create(scheduleList, dailyScheduleList);
        }

        private Tuple<List<ILoanScheduleDetail>, List<ILoanScheduleDetail>> GetInterestOnlyFollowedByBalloonActualScheduleForNonSCF(IScheduleInputDetails scheduleInputDetails, double annualRate)
        {
            var scheduleList = new List<ILoanScheduleDetail>();
            var dailyScheduleList = new List<ILoanScheduleDetail>();
            var firstBillingDate = scheduleInputDetails.OriginalStartDate.Date;
            var onBoardingDate = scheduleInputDetails.LoanStartDate.Date;
            var amortStartDate = scheduleInputDetails.LoanStartDate.Date;
            var nextBillingDate = scheduleInputDetails.OriginalStartDate.Date;
            var loanAmount = scheduleInputDetails.PrincipalOutStanding;
            var firstPaymentLastPayment = false;
            var isLastPayment = false;
            var scheduleDetails = new LoanScheduleDetail();

            for (int i = 0; i < scheduleInputDetails.Term; i++)
            {
                if (amortStartDate.Date == onBoardingDate.Date)
                {
                    if (scheduleInputDetails.Term == 1)
                    {
                        isLastPayment = true;
                    }
                    scheduleDetails.Installmentnumber = 1;
                    scheduleDetails.InterestHandlingType = InterestHandlingType.InterestOnly;
                    scheduleDetails.OpeningPrincipalOutStandingBalance = loanAmount;
                    scheduleDetails.OriginalScheduleDate = scheduleInputDetails.OriginalStartDate.Date;
                    scheduleDetails.PrincipalAmount = 0;
                    scheduleDetails.ClosingPrincipalOutStandingBalance = loanAmount;
                    scheduleDetails.MinimumAmountDue = RoundOff.Round((scheduleDetails.OpeningPrincipalOutStandingBalance * scheduleInputDetails.MADPercentage) / 100, scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);
                    for (DateTime j = amortStartDate.Date; j < scheduleDetails.OriginalScheduleDate; j = j.AddDays(1))
                    {
                        var days = Helper.GetDaysInYearForProvidedDate(j, firstBillingDate);
                        var dailyInterestRate = InterestRate.Calculate_InterestRate("daily", annualRate,
                            Convert.ToInt32(days));

                        var dailyScheduleDetails = new LoanScheduleDetail();
                        dailyScheduleDetails.Installmentnumber = scheduleDetails.Installmentnumber;
                        dailyScheduleDetails.InterestAmount = dailyInterestRate * 1 * loanAmount / 100;
                        dailyScheduleDetails.CumulativeInterestAmount =
                            dailyScheduleDetails.InterestAmount + (dailyScheduleList.Any() ?
                                dailyScheduleList.OrderByDescending(l => l.ScheduleDate)
                                .Select(l => l.CumulativeInterestAmount).FirstOrDefault() : 0);
                        dailyScheduleDetails.PrincipalAmount = 0;
                        dailyScheduleDetails.PaymentAmount = 0;
                        dailyScheduleDetails.ScheduleDate = j;
                        dailyScheduleDetails.OriginalScheduleDate = j;
                        dailyScheduleList.Add(dailyScheduleDetails);
                    }
                    if (firstBillingDate.Date != scheduleInputDetails.PaymentStartDate.Date)
                    {
                        scheduleDetails.ScheduleDate = scheduleInputDetails.PaymentStartDate;
                    }
                    else
                    {
                        scheduleDetails.ScheduleDate = firstBillingDate;
                    }
                    scheduleDetails.InterestAmount = RoundOff.Round(dailyScheduleList
                        .Where(x => x.Installmentnumber == scheduleDetails.Installmentnumber)
                        .Sum(x => x.InterestAmount) + scheduleInputDetails.RemainingInterestAmount,
                        scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);

                    scheduleDetails.PrincipalAmount = isLastPayment ? loanAmount : 0;
                    scheduleDetails.PaymentAmount = RoundOff.Round(
                        dailyScheduleList.Where(x => x.Installmentnumber == scheduleDetails.Installmentnumber)
                        .Sum(x => x.InterestAmount) + scheduleDetails.PrincipalAmount + scheduleInputDetails.RemainingInterestAmount,
                        scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);

                    scheduleDetails.CumulativeInterestAmount = scheduleDetails.InterestAmount;
                    scheduleDetails.CumulativePrincipalAmount = RoundOff.Round(
                        isLastPayment ? loanAmount : 0,
                        scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);
                    scheduleDetails.CumulativePaymentAmount = RoundOff.Round(
                        scheduleDetails.CumulativeInterestAmount + scheduleDetails.CumulativePrincipalAmount,
                        scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);
                    scheduleDetails.InterestHandlingType = scheduleInputDetails.InterestHandlingType;

                    scheduleDetails.ClosingPrincipalOutStandingBalance =
                        scheduleDetails.OpeningPrincipalOutStandingBalance - scheduleDetails.PrincipalAmount;
                    scheduleDetails.SurplusAmount = scheduleInputDetails.RemainingInterestAmount;
                    scheduleList.Add(scheduleDetails);
                    amortStartDate = firstBillingDate.Date;
                    continue;
                }

                nextBillingDate = GetNextScheduleDate(scheduleInputDetails.PaymentFrequency, nextBillingDate);

                var scheduleDetailOthers = new LoanScheduleDetail();

                scheduleDetailOthers.Installmentnumber = scheduleList.OrderByDescending(k => k.ScheduleDate)
                    .FirstOrDefault().Installmentnumber + 1;
                if (scheduleInputDetails.Term == scheduleDetailOthers.Installmentnumber)
                {
                    if (firstPaymentLastPayment == true)
                    {
                        return Tuple.Create(scheduleList, dailyScheduleList);
                    }
                    isLastPayment = true;
                }
                scheduleDetailOthers.InterestHandlingType = isLastPayment ? InterestHandlingType.InterestPrincipal :
                    InterestHandlingType.InterestOnly;
                scheduleDetailOthers.OpeningPrincipalOutStandingBalance = loanAmount;
                scheduleDetailOthers.ScheduleDate = nextBillingDate;
                scheduleDetailOthers.PrincipalAmount = isLastPayment ? loanAmount : 0;
                scheduleDetailOthers.ClosingPrincipalOutStandingBalance = isLastPayment ? 0 : loanAmount;
                scheduleDetailOthers.OriginalScheduleDate = nextBillingDate;
                var nextBillingDateFrom = CalendarService.GetDate(
                    nextBillingDate.Year, nextBillingDate.Month, nextBillingDate.Day);

                if (nextBillingDateFrom.Type == DateType.Holiday || nextBillingDateFrom.Type == DateType.Weekend)
                {
                    scheduleDetailOthers.ScheduleDate = nextBillingDateFrom.NextBusinessDay.Date;
                }

                for (DateTime j = amortStartDate.Date; j < scheduleDetailOthers.OriginalScheduleDate; j = j.AddDays(1))
                {
                    var days = Helper.GetDaysInYearForProvidedDate(
                        j, scheduleDetailOthers.OriginalScheduleDate);
                    var dailyInterestRate = InterestRate.Calculate_InterestRate(
                        "daily", annualRate, Convert.ToInt32(days));

                    var dailyScheduleDetails = new LoanScheduleDetail();
                    dailyScheduleDetails.Installmentnumber = scheduleDetailOthers.Installmentnumber;
                    dailyScheduleDetails.InterestAmount = dailyInterestRate * 1 * loanAmount / 100;
                    dailyScheduleDetails.CumulativeInterestAmount =
                        dailyScheduleDetails.InterestAmount + (dailyScheduleList.Any() ?
                            dailyScheduleList.OrderByDescending(l => l.ScheduleDate)
                            .Select(l => l.CumulativeInterestAmount).FirstOrDefault() : 0);
                    dailyScheduleDetails.PrincipalAmount = 0;
                    dailyScheduleDetails.PaymentAmount = 0;
                    dailyScheduleList.Add(dailyScheduleDetails);

                }
                scheduleDetailOthers.InterestAmount = RoundOff.Round(
                    dailyScheduleList.Where(x => x.Installmentnumber == scheduleDetailOthers.Installmentnumber)
                    .Sum(x => x.InterestAmount),
                    scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);

                //var additionalInterest = RoundOff.Round(GetInterestBasedOnAdjustType(scheduleInputDetails.InterestAdjustmentType, scheduleInputDetails.RemainingInterestAmount, scheduleInputDetails.Term, lastSchdeuleDetail.InstallmentNumber), scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);

                scheduleDetailOthers.InterestAmount = scheduleDetailOthers.InterestAmount; // + additionalInterest;

                scheduleDetailOthers.PaymentAmount = RoundOff.Round(
                    dailyScheduleList.Where(x => x.Installmentnumber == scheduleDetailOthers.Installmentnumber)
                    .Sum(x => x.InterestAmount) + scheduleDetailOthers.PrincipalAmount,
                    scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);

                scheduleDetailOthers.CumulativeInterestAmount = RoundOff.Round(dailyScheduleList
                    .Sum(x => x.InterestAmount), scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);
                scheduleDetailOthers.CumulativePrincipalAmount = isLastPayment == true ? loanAmount : 0;
                scheduleDetailOthers.CumulativePaymentAmount = RoundOff.Round(
                    scheduleDetailOthers.CumulativeInterestAmount + scheduleDetailOthers.CumulativePrincipalAmount,
                    scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);
                scheduleDetailOthers.InterestHandlingType = scheduleInputDetails.InterestHandlingType;

                scheduleDetailOthers.ClosingPrincipalOutStandingBalance = scheduleDetailOthers.OpeningPrincipalOutStandingBalance - scheduleDetailOthers.PrincipalAmount;
                scheduleDetailOthers.MinimumAmountDue = RoundOff.Round((scheduleDetails.OpeningPrincipalOutStandingBalance * scheduleInputDetails.MADPercentage) / 100, scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod); ;
                scheduleList.Add(scheduleDetailOthers);
                amortStartDate = nextBillingDate;

            }

            return Tuple.Create(scheduleList, dailyScheduleList);

        }

        #endregion

    }
}