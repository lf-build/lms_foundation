﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using LendFoundry.Calendar.Client;
using LMS.Foundation.Calculus;

namespace LMS.Foundation.Amortization
{
    internal partial class LoanAmortization : ILoanAmortization
    {

        private DateTime ScheduleDate = DateTime.MinValue;

        public ILoanAmortizationResponse GetAmortizationSchedule(IAmortizationRequest amortizationRequest)
        {
            try
            {
                ValidateAmortizationRequest(amortizationRequest);
                SetDefaultValues(ref amortizationRequest);

                switch (GetPortfolioType(amortizationRequest.PortfolioType))
                {
                    case PortfolioType.Installment:
                    case PortfolioType.LineOfCredit:
                    case PortfolioType.Mortgage:
                    case PortfolioType.Open:
                    case PortfolioType.Revolving:
                    case PortfolioType.SCF:
                        var schedule = GetNewSchedule(amortizationRequest, GetRequiredParametersForSchedule(amortizationRequest));
                        return new LoanAmortizationResponse() { LoanScheduleDetail = schedule };
                    default:
                        throw new InvalidEnumArgumentException($"Invalid PortfolioType : {amortizationRequest.PortfolioType}");
                }
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException(ex.Message, ex);
            }
        }

        private List<ILoanScheduleDetail> GetNewSchedule(IAmortizationRequest amortizationRequest, IScheduleInputDetails scheduleInputDetails)
        {
            var ScheduleList = new List<LoanScheduleDetail>();
            var lastScheduleDetail = new LastScheduleDetails();
            InitializeScheduleDetails(scheduleInputDetails, lastScheduleDetail);
            var internalAmortizationDetails = InternalAmortizationCalculations(amortizationRequest, lastScheduleDetail.LastScheduleDate);

            foreach (var stream in scheduleInputDetails.PaymentStream)
            {
                if (scheduleInputDetails.PaymentStream.Count() == 1 && internalAmortizationDetails.LoanOriginalEndDate.HasValue)
                {
                    stream.NoOfPayments = internalAmortizationDetails.TermBasedOnLoanEndDate;
                }
                else if (scheduleInputDetails.PaymentStream.Count() == 1 && internalAmortizationDetails.BrokenPeriodDays == 0 && amortizationRequest.BrokenPeriodHandlingType.ToLower() == "separate" && amortizationRequest.PaymentFrequency != PaymentFrequency.Daily)
                {
                    // TODO: Considering currently for normal schedule building only and not for loan modifications
                    stream.NoOfPayments = stream.NoOfPayments - 1;
                }
                for (int i = 0; i < stream.NoOfPayments; i++)
                {

                    var scheduleDetails = new LoanScheduleDetail();
                    scheduleDetails.Installmentnumber = lastScheduleDetail.InstallmentNumber;
                    if (scheduleDetails.Installmentnumber == 1)
                    {
                        scheduleDetails.OriginalScheduleDate = lastScheduleDetail.LastScheduleDate;
                        scheduleDetails.ScheduleDate = lastScheduleDetail.LastNonHolidayScheduleDate;
                    }
                    else
                    {
                        var scheduleDates = GetScheduleDates(amortizationRequest, lastScheduleDetail);
                        scheduleDetails.OriginalScheduleDate = scheduleDates.Item1;
                        scheduleDetails.ScheduleDate = scheduleDates.Item2;
                    }
                    
                    if (amortizationRequest.ScheduleType == ScheduleType.InterestOnlyFollowedByBalloon && internalAmortizationDetails.LoanEndDate.HasValue && scheduleDetails.OriginalScheduleDate >= internalAmortizationDetails.LoanOriginalEndDate)
                    {
                        scheduleDetails.OriginalScheduleDate = internalAmortizationDetails.LoanOriginalEndDate.Value;
                        scheduleDetails.ScheduleDate = internalAmortizationDetails.LoanEndDate.Value;
                    }
                    
                    if (scheduleDetails.Installmentnumber == 1)
                    {
                        if (amortizationRequest.BrokenPeriodHandlingType.ToLower() == "firstemi" || amortizationRequest.BrokenPeriodHandlingType.ToLower() == "ignore" || internalAmortizationDetails.BrokenPeriodDays <= 0)
                        {
                            scheduleDetails.FrequencyDays = GetNumberOfDaysInAFrequency(amortizationRequest.DailyAccrualMethod, amortizationRequest.PaymentFrequency);
                        }
                        else
                        {
                            scheduleDetails.FrequencyDays = Convert.ToInt32((scheduleDetails.OriginalScheduleDate - amortizationRequest.FundedDate).TotalDays);
                        }
                    }
                    else
                    {
                        if (amortizationRequest.ScheduleType == ScheduleType.InterestOnlyFollowedByBalloon && internalAmortizationDetails.LoanEndDate.HasValue && scheduleDetails.OriginalScheduleDate >= internalAmortizationDetails.LoanOriginalEndDate.Value)
                        {
                            scheduleDetails.FrequencyDays = Convert.ToInt32((scheduleDetails.OriginalScheduleDate - lastScheduleDetail.LastScheduleDate).TotalDays);
                        }
                        else
                        {
                            scheduleDetails.FrequencyDays = GetNumberOfDaysInAFrequency(amortizationRequest.DailyAccrualMethod, amortizationRequest.PaymentFrequency);
                        }
                    }

                    scheduleDetails.OpeningPrincipalOutStandingBalance = RoundOff.Round(lastScheduleDetail.LastPrincipalOutStandingBalance, amortizationRequest.RoundingDigit, amortizationRequest.RoundingMethod);

                    //payment details
                    scheduleDetails.InterestAmount = RoundOff.Round(GetCalculatedInterestAmount(amortizationRequest, lastScheduleDetail, scheduleDetails, internalAmortizationDetails), amortizationRequest.RoundingDigit, amortizationRequest.RoundingMethod);
                    scheduleDetails.PrincipalAmount = RoundOff.Round(GetCalculatedPrincipalAmount(amortizationRequest, scheduleDetails, scheduleDetails.OpeningPrincipalOutStandingBalance, scheduleDetails.InterestAmount, stream.InstallmentAmount, scheduleDetails.Installmentnumber, stream.NoOfPayments, internalAmortizationDetails), amortizationRequest.RoundingDigit, amortizationRequest.RoundingMethod);
                    scheduleDetails.PaymentAmount = RoundOff.Round(GetCalculatedPaymentAmount(scheduleDetails.PrincipalAmount, scheduleDetails.InterestAmount), amortizationRequest.RoundingDigit, amortizationRequest.RoundingMethod);

                    //Cumulative details
                    scheduleDetails.CumulativeInterestAmount = RoundOff.Round(lastScheduleDetail.CumulativeInterestAmount + scheduleDetails.InterestAmount, amortizationRequest.RoundingDigit, amortizationRequest.RoundingMethod);
                    scheduleDetails.CumulativePrincipalAmount = RoundOff.Round(lastScheduleDetail.CumulativePrincipalAmount + scheduleDetails.PrincipalAmount, amortizationRequest.RoundingDigit, amortizationRequest.RoundingMethod);
                    scheduleDetails.CumulativePaymentAmount = RoundOff.Round(lastScheduleDetail.CumulativePaymentAmount + scheduleDetails.PaymentAmount, amortizationRequest.RoundingDigit, amortizationRequest.RoundingMethod);

                    scheduleDetails.ClosingPrincipalOutStandingBalance = RoundOff.Round(scheduleDetails.OpeningPrincipalOutStandingBalance - scheduleDetails.PrincipalAmount, amortizationRequest.RoundingDigit, amortizationRequest.RoundingMethod);


                    ScheduleList.Add(scheduleDetails);
                    //update last Schedule info
                    UpdateLastScheduleInfo(lastScheduleDetail, scheduleDetails);
                }
            }
            return new List<ILoanScheduleDetail>(ScheduleList);
        }

        private IScheduleInputDetails GetRequiredParametersForSchedule(IAmortizationRequest amortizationRequest)
        {
            var param = new ScheduleInputDetails();
            param.PaymentFrequency = amortizationRequest.PaymentFrequency;
            param.MaturityDays = 30 * amortizationRequest.Term;
            param.OriginalPaymentStartDate = amortizationRequest.OriginalFirstPaymentDate;
            var paymentStartDate = CalendarService.GetDate(amortizationRequest.OriginalFirstPaymentDate.Year, amortizationRequest.OriginalFirstPaymentDate.Month, amortizationRequest.OriginalFirstPaymentDate.Day);
            param.PaymentStartDate = paymentStartDate.Type == DateType.Holiday || paymentStartDate.Type == DateType.Weekend ? paymentStartDate.NextBusinessDay.Date : amortizationRequest.OriginalFirstPaymentDate;
            param.RoundingDigit = amortizationRequest.RoundingDigit;
            param.RoundingMethod = amortizationRequest.RoundingMethod;
            param.PrincipalOutStanding = amortizationRequest.LoanAmount;

            if (amortizationRequest.PaymentStream != null)
            {
                foreach (var stream in amortizationRequest.PaymentStream)
                {
                    stream.AppliedInterestRate = InterestRate.Calculate_InterestRate(stream.PaymentFrequency.ToString(), amortizationRequest.InterestRate);
                    var streamAmount = amortizationRequest.LoanAmount * stream.RepayablePercent / 100;
                    stream.InstallmentAmount = amortizationRequest.PaymentAmount > 0 ? amortizationRequest.PaymentAmount : RoundOff.Round(PMT.Calculate_PMT(streamAmount, stream.NoOfPayments, stream.AppliedInterestRate), 2, param.RoundingMethod);
                }
            }
            var paymentStream = new List<PaymentStream>();
            if (amortizationRequest.PaymentStream == null)
            {
                var appliedRate = InterestRate.Calculate_InterestRate(amortizationRequest.PaymentFrequency.ToString(), amortizationRequest.InterestRate);
                var installmentAmount = amortizationRequest.PaymentAmount > 0 ? amortizationRequest.PaymentAmount : RoundOff.Round(PMT.Calculate_PMT(amortizationRequest.LoanAmount, amortizationRequest.Term, appliedRate), param.RoundingDigit, param.RoundingMethod);
                paymentStream = new List<PaymentStream>() { new PaymentStream() { NoOfPayments = (amortizationRequest.BrokenPeriodHandlingType.ToLower() == "separate" && amortizationRequest.PaymentFrequency != PaymentFrequency.Daily ? amortizationRequest.Term + 1 : amortizationRequest.Term), PaymentFrequency = param.PaymentFrequency, RepayablePercent = 100, AppliedInterestRate = appliedRate, InstallmentAmount = installmentAmount } };
            }

            param.PaymentStream = amortizationRequest.PaymentStream != null && amortizationRequest.PaymentStream.Count > 0
                                    ? amortizationRequest.PaymentStream
                                    : paymentStream;

            param.ActualAmortizationType = amortizationRequest.ActualAmortizationType;
            param.DailyAccrualMethod = amortizationRequest.DailyAccrualMethod;
            param.Term = (amortizationRequest.BrokenPeriodHandlingType.ToLower() == "separate" && amortizationRequest.PaymentFrequency != PaymentFrequency.Daily ? amortizationRequest.Term + 1 : amortizationRequest.Term);
            return param;
        }

        private double GetCalculatedInterestAmount(IAmortizationRequest amortizationRequest, LastScheduleDetails lastScheduleDetail, ILoanScheduleDetail loanScheduleDetail, InternalAmortizationDetails internalAmortizationDetails)
        {
            var brokenPeriodInterest = GetBrokenPeriodInterest(amortizationRequest, internalAmortizationDetails, lastScheduleDetail, loanScheduleDetail);
            if ((amortizationRequest.BrokenPeriodHandlingType.ToLower() == "fullemi" || amortizationRequest.BrokenPeriodHandlingType.ToLower() == "separate") && loanScheduleDetail.Installmentnumber == 1 && amortizationRequest.PaymentFrequency != PaymentFrequency.Daily && internalAmortizationDetails.BrokenPeriodDays > 0)
            {
                return brokenPeriodInterest;
            }
            else
            {
                var interestAmount = GetDailyRate(amortizationRequest.DailyAccrualMethod, internalAmortizationDetails.FrequencyRate, amortizationRequest.PaymentFrequency, loanScheduleDetail.OriginalScheduleDate, loanScheduleDetail.Installmentnumber == 1 ? amortizationRequest.FundedDate : lastScheduleDetail.LastScheduleDate) * loanScheduleDetail.FrequencyDays * lastScheduleDetail.LastPrincipalOutStandingBalance;
                return interestAmount + brokenPeriodInterest;
            }
        }

        private double GetBrokenPeriodInterest(IAmortizationRequest amortizationRequest, InternalAmortizationDetails internalAmortizationDetails, LastScheduleDetails lastScheduleDetail, ILoanScheduleDetail loanScheduleDetail)
        {
            var brokenPeriodInterest = 0.0;
            if ((amortizationRequest.BrokenPeriodHandlingType.ToLower() == "fullemi" || amortizationRequest.BrokenPeriodHandlingType.ToLower() == "separate") && loanScheduleDetail.Installmentnumber == 1)
            {
                brokenPeriodInterest = internalAmortizationDetails.BrokenPeriodDailyRate * internalAmortizationDetails.BrokenPeriodDays * lastScheduleDetail.LastPrincipalOutStandingBalance;
            }
            else
            {
                if (amortizationRequest.BrokenPeriodHandlingType.ToLower() != "ignore")
                {
                    if (loanScheduleDetail.Installmentnumber == 1)
                    {
                        brokenPeriodInterest = internalAmortizationDetails.BrokenPeriodDays * internalAmortizationDetails.BrokenPeriodDailyRate * loanScheduleDetail.OpeningPrincipalOutStandingBalance;
                    }
                }
            }
            return brokenPeriodInterest;
        }

        private double GetCalculatedPrincipalAmount(IAmortizationRequest amortizationRequest, ILoanScheduleDetail loanScheduleDetail, double outstandingPrincipal, double interestAmount, double installmentAmount, int installmentNumber, int term, InternalAmortizationDetails internalAmortizationDetails)
        {
            if (amortizationRequest.ScheduleType == ScheduleType.InterestOnlyFollowedByBalloon && !IsLastPayment(installmentNumber, term))
            {
                return 0;
            }
            else
            {
                if (amortizationRequest.BrokenPeriodHandlingType.ToLower() == "separate" && loanScheduleDetail.Installmentnumber == 1 && amortizationRequest.PaymentFrequency != PaymentFrequency.Daily && internalAmortizationDetails.BrokenPeriodDays > 0)
                {
                    return 0;
                }
                else if (amortizationRequest.BrokenPeriodHandlingType.ToLower() == "fullemi" && loanScheduleDetail.Installmentnumber == 1)
                {
                    return installmentAmount - interestAmount;
                }
                else
                {
                    if (IsLastPayment(installmentNumber, term))
                    {
                        return outstandingPrincipal;
                    }
                    else
                    {
                        return installmentAmount - interestAmount;
                    }
                }
            }

        }

        private double GetCalculatedPaymentAmount(double principalAmount, double interestAmount)
        {
            return principalAmount + interestAmount;
        }

        private double GetDailyRate(string dailyAccrualMethod, double frequencyRate, PaymentFrequency paymentFrequency, DateTime scheduleDate, DateTime previousScheduleDate)
        {
            if (dailyAccrualMethod == "30/360")
            {
                switch (paymentFrequency)
                {
                    case PaymentFrequency.Daily:
                        return frequencyRate / 1;
                    case PaymentFrequency.Weekly:
                        return frequencyRate / 7;
                    case PaymentFrequency.BiWeekly:
                        return frequencyRate / 14;
                    case PaymentFrequency.Monthly:
                        return frequencyRate / 30;
                    case PaymentFrequency.Quarterly:
                        return frequencyRate / 120;
                    case PaymentFrequency.SemiYearly:
                        return frequencyRate / 180;
                    case PaymentFrequency.Yearly:
                        return frequencyRate / 360;
                    default:
                        throw new ArgumentException($"Invalid payment frequency {paymentFrequency}");
                }
            }
            // TODO: Add all other DailyAccrualMethod conditions
            return 0;
        }

        private Tuple<DateTime, DateTime> GetScheduleDates(IAmortizationRequest amortizationRequest, LastScheduleDetails lastScheduleDetail)
        {
            if (amortizationRequest == null || lastScheduleDetail == null)
            {
                throw new ArgumentException($"Not able to generate schedule");
            }
            var scheduleDate = GetNextScheduleDate(amortizationRequest.PaymentFrequency, lastScheduleDetail.LastScheduleDate);
            if (ScheduleDate == DateTime.MinValue)
            {
                ScheduleDate = scheduleDate;
            }
            if (scheduleDate.Date <= lastScheduleDetail.LastNonHolidayScheduleDate.Date || scheduleDate.Date <= lastScheduleDetail.LastScheduleDate.Date)
            {
                lastScheduleDetail.LastScheduleDate = scheduleDate.Date;
                return GetScheduleDates(amortizationRequest, lastScheduleDetail);
            }
            var originalScheduleDate = ScheduleDate;
            if (!amortizationRequest.IsHolidaySchedule)
            {
                var date = CalendarService.GetDate(scheduleDate.Year, scheduleDate.Month, scheduleDate.Day);
                if (date.Type == DateType.Holiday || date.Type == DateType.Weekend)
                {
                    scheduleDate = date.NextBusinessDay.Date;
                }
            }
            ScheduleDate = DateTime.MinValue;
            return Tuple.Create(originalScheduleDate, scheduleDate);
        }

        private static void ValidateAmortizationRequest(IAmortizationRequest amortizationRequest)
        {
            if (amortizationRequest == null)
                throw new ArgumentException($"#{nameof(amortizationRequest)} cannot be null", nameof(amortizationRequest));

            if (string.IsNullOrWhiteSpace(amortizationRequest.PortfolioType))
                throw new ArgumentException($"#{nameof(amortizationRequest.PortfolioType)} cannot be null", nameof(amortizationRequest.PortfolioType));

            if (amortizationRequest.LoanAmount <= 0)
                throw new ArgumentException($"#{nameof(amortizationRequest.LoanAmount)} should be greater than zero");

            if (!Enum.IsDefined(typeof(PaymentFrequency), amortizationRequest.PaymentFrequency))
                throw new ArgumentException($"#{nameof(amortizationRequest.PaymentFrequency)} cannot be null", nameof(amortizationRequest.PaymentFrequency));

            if (amortizationRequest.InterestRate <= 0)
                throw new ArgumentException($"#{nameof(amortizationRequest.InterestRate)} cannot be null", nameof(amortizationRequest.InterestRate));

            if (amortizationRequest.Term <= 0)
                throw new ArgumentException($"#{nameof(amortizationRequest.Term)} cannot be null", nameof(amortizationRequest.Term));

            if (amortizationRequest.FundedDate == DateTime.MinValue)
                throw new ArgumentException($"#{nameof(amortizationRequest.FundedDate)} cannot be null", nameof(amortizationRequest.FundedDate));
        }

        private void SetDefaultValues(ref IAmortizationRequest amortizationRequest)
        {
            if (amortizationRequest == null)
                return;

            if (string.IsNullOrWhiteSpace(amortizationRequest.ActualAmortizationType))
            {
                amortizationRequest.ActualAmortizationType = "VariableDailyRate";
            }
            if (string.IsNullOrWhiteSpace(amortizationRequest.DailyAccrualMethod))
            {
                amortizationRequest.DailyAccrualMethod = "30/360";
            }
            if (string.IsNullOrWhiteSpace(amortizationRequest.BrokenPeriodInterestType))
            {
                amortizationRequest.BrokenPeriodInterestType = "30/360";
            }
            if (string.IsNullOrWhiteSpace(amortizationRequest.BrokenPeriodHandlingType))
            {
                amortizationRequest.BrokenPeriodHandlingType = "Ignore";
            }
            if (string.IsNullOrWhiteSpace(amortizationRequest.CarryForwardOutstanding))
            {
                amortizationRequest.CarryForwardOutstanding = "PrincipalOnly";
            }
            if (string.IsNullOrWhiteSpace(amortizationRequest.RoundingMethod))
            {
                amortizationRequest.RoundingMethod = "mathematical";
            }
            if (amortizationRequest.RoundingDigit < 0)
            {
                amortizationRequest.RoundingDigit = 2;
            }
        }

        private InternalAmortizationDetails InternalAmortizationCalculations(IAmortizationRequest amortizationRequest, DateTime firstPaymentDate)
        {
            var frequencyRate = (amortizationRequest.InterestRate / 100) / GetFrequencyInAYearBasedOnParameters(amortizationRequest.DailyAccrualMethod, amortizationRequest.PaymentFrequency);
            var dailyRate = frequencyRate / GetNumberOfDaysInAFrequency(amortizationRequest.DailyAccrualMethod, amortizationRequest.PaymentFrequency);
            var brokenPeriodDays = 0;
            var brokenPeriodDailyRate = 0.0;
            var previousExpectedDate = GetPreviousExpectedDate(amortizationRequest.PaymentFrequency, firstPaymentDate);
            if (amortizationRequest.FundedDate.Date < previousExpectedDate.Date)
            {
                brokenPeriodDays = Convert.ToInt32((previousExpectedDate.Date - amortizationRequest.FundedDate.Date).TotalDays);
                brokenPeriodDailyRate = (amortizationRequest.BrokenPeriodInterestType.ToLower() == "variable" ? frequencyRate : amortizationRequest.InterestRate / 100) / GetBrokenPeriodDenominator(amortizationRequest.BrokenPeriodInterestType, amortizationRequest.PaymentFrequency, GetPreviousExpectedDate(amortizationRequest.PaymentFrequency, previousExpectedDate));
            }
            else if (amortizationRequest.FundedDate.Date > previousExpectedDate.Date)
            {
                brokenPeriodDays = Convert.ToInt32((firstPaymentDate.Date - amortizationRequest.FundedDate.Date).TotalDays);
                brokenPeriodDailyRate = (amortizationRequest.BrokenPeriodInterestType.ToLower() == "variable" ? frequencyRate : amortizationRequest.InterestRate / 100) / GetBrokenPeriodDenominator(amortizationRequest.BrokenPeriodInterestType, amortizationRequest.PaymentFrequency, firstPaymentDate);
            }

            var loanEndDate = amortizationRequest.MaturityTermDays > 0
                                            ? amortizationRequest.FundedDate.AddDays(amortizationRequest.MaturityTermDays)
                                            : (DateTime?)null;
            var term = 1;
            if (loanEndDate.HasValue)
            {
                GetTermBasedOnEndDate(firstPaymentDate, loanEndDate.Value, amortizationRequest.PaymentFrequency, ref term);
            }
            if (loanEndDate != null)
            {
                var date = CalendarService.GetDate(loanEndDate.Value.Year, loanEndDate.Value.Month, loanEndDate.Value.Day);
                if ((date.Type == DateType.Holiday || date.Type == DateType.Weekend) && !amortizationRequest.IsHolidaySchedule)
                {
                    return new InternalAmortizationDetails()
                    {
                        FrequencyRate = frequencyRate,
                        DailyRate = dailyRate,
                        BrokenPeriodDays = brokenPeriodDays,
                        BrokenPeriodDailyRate = brokenPeriodDailyRate,
                        LoanOriginalEndDate = loanEndDate,
                        LoanEndDate = date.NextBusinessDay.Date,
                        TermBasedOnLoanEndDate = term
                    };
                }
            }
            return new InternalAmortizationDetails()
            {
                FrequencyRate = frequencyRate,
                DailyRate = dailyRate,
                BrokenPeriodDays = brokenPeriodDays,
                BrokenPeriodDailyRate = brokenPeriodDailyRate,
                LoanEndDate = loanEndDate,
                LoanOriginalEndDate = loanEndDate,
                TermBasedOnLoanEndDate = term
            };
        }

        private int GetFrequencyInAYearBasedOnParameters(string dailyAccrualMethod, PaymentFrequency paymentFrequency)
        {
            if (dailyAccrualMethod == "30/360")
            {
                switch (paymentFrequency)
                {
                    case PaymentFrequency.Daily:
                        return 360;
                    case PaymentFrequency.Weekly:
                        return 52;
                    case PaymentFrequency.Monthly:
                        return 12;
                    case PaymentFrequency.Quarterly:
                        return 4;
                    case PaymentFrequency.SemiYearly:
                        return 2;
                    case PaymentFrequency.Yearly:
                        return 1;
                }
            }
            // TODO: Add all other DailyAccrualMethod conditions
            return 0;
        }

        private int GetNumberOfDaysInAFrequency(string dailyAccrualMethod, PaymentFrequency paymentFrequency)
        {
            if (dailyAccrualMethod == "30/360")
            {
                switch (paymentFrequency)
                {
                    case PaymentFrequency.Daily:
                        return 1;
                    case PaymentFrequency.Weekly:
                        return 7;
                    case PaymentFrequency.Monthly:
                        return 30;
                    case PaymentFrequency.Quarterly:
                        return 120;
                    case PaymentFrequency.SemiYearly:
                        return 180;
                    case PaymentFrequency.Yearly:
                        return 360;
                }
            }
            // TODO: Add all other DailyAccrualMethod conditions
            return 0;
        }

        private double GetBrokenPeriodDenominator(string brokenPeriodInterestType, PaymentFrequency paymentFrequency, DateTime firstPaymentDate)
        {
            if (brokenPeriodInterestType.ToLower() == "30/360")
            {
                return 360;
            }
            else if (brokenPeriodInterestType.ToLower() == "actual/365")
            {
                return 365;
            }
            else if (brokenPeriodInterestType.ToLower() == "actual/actual")
            {
                return (firstPaymentDate.AddYears(1) - firstPaymentDate).TotalDays;
            }
            else
            {
                var previousExpectedDate = GetPreviousExpectedDate(paymentFrequency, firstPaymentDate);
                return (firstPaymentDate.Date - previousExpectedDate.Date).TotalDays;
            }
        }

        private DateTime GetPreviousExpectedDate(PaymentFrequency paymentFrequency, DateTime firstPaymentDate)
        {
            switch (paymentFrequency)
            {
                case PaymentFrequency.Daily:
                    return firstPaymentDate.AddDays(-1);
                case PaymentFrequency.Weekly:
                    return firstPaymentDate.AddDays(-7);
                case PaymentFrequency.Monthly:
                    return firstPaymentDate.AddMonths(-1);
                case PaymentFrequency.Quarterly:
                    return firstPaymentDate.AddMonths(-4);
                case PaymentFrequency.SemiYearly:
                    return firstPaymentDate.AddMonths(-6);
                case PaymentFrequency.Yearly:
                    return firstPaymentDate.AddYears(-1);
                default:
                    throw new ArgumentException($"Invalid payment frequency: {paymentFrequency}");
            }
        }

        private void GetTermBasedOnEndDate(DateTime firstPaymentDate, DateTime loanEndDate, PaymentFrequency paymentFrequency, ref int term)
        {
            if (firstPaymentDate.Date < loanEndDate.Date)
            {
                var date = GetNextScheduleDate(paymentFrequency, firstPaymentDate);
                if (date.Date > loanEndDate.Date)
                {
                    term += 1;
                }
                else
                {
                    term += 1;
                    GetTermBasedOnEndDate(date, loanEndDate, paymentFrequency, ref term);
                }
            }
        }
    }
}