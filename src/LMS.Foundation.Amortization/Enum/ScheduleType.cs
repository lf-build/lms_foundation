namespace LMS.Foundation.Amortization
{
    public enum ScheduleType
    {
        None,
        EquatedMonthlyInstalments,
        InterestOnlyFollowedByBalloon
    }
}