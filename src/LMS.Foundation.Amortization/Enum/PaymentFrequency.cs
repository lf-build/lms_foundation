﻿namespace LMS.Foundation.Amortization
{
    public enum PaymentFrequency
    {
        Daily,
        Weekly,
        BiWeekly,
        Monthly,
        Quarterly,
        SemiYearly,
        Yearly
        
    }
}
