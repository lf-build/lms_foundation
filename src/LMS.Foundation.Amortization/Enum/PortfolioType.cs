﻿namespace LMS.Foundation.Amortization
{
    public enum PortfolioType
    {
        Installment,
        Mortgage,
        LineOfCredit,
        Open,
        Revolving,
        MCA,
        MCALOC,

        SCF
    }
}
