﻿namespace LMS.Foundation.Amortization
{
    public enum InterestHandlingType
    {
        None,
        InterestOnly,
        InterestPrincipal,
        PartialInterest
    }
}

    