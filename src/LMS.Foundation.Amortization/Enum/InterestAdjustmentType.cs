﻿
namespace LMS.Foundation.Amortization
{
    public enum InterestAdjustmentType
    {
        None,
        FirstSchedule,
        LastSchedule,
        FirstInterestOnlySchedule,
        LastInterestOnlySchedule,
        DistributedSchedule
    }
}
