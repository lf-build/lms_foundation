﻿
using LendFoundry.Foundation.Date;
using System;
using System.Collections.Generic;

namespace LMS.Foundation.Amortization
{
    public static class Program
    {
        public static void Main(string[] args)
        {

            // var requestParam = new LoanAmortizationRequest();
            // requestParam.LoanAmount = 20000;
            // // requestParam.InterestRate = 8;
            // requestParam.FactorRate = 1.39;
            // //requestParam.PaymentAmount = 100;
            // requestParam.PaymentFrequency = PaymentFrequency.Daily;
            // requestParam.Term = 126;
            // requestParam.PaymentStartDate = new DateTime(2018, 03, 01);
            // requestParam.OriginalStartDate = new DateTime(2018, 03, 01);
            // requestParam.IsHolidaySchedule = false;
            // requestParam.PortfolioType = "mca";
            // requestParam.RoundingDigit = 2;
            // requestParam.RoundingMethod = "bankers";
            // requestParam.LoanStartDate = new DateTime(2018, 02, 15); ;
            // requestParam.InterestHandlingType = InterestHandlingType.None;
            // requestParam.InterestAdjustmentType = InterestAdjustmentType.None;

            // requestParam.SlabDetails = new List<SlabDetails>();
            // requestParam.SlabDetails.Add(new SlabDetails() { MonthSequence = 1, Percentage = 26 });
            // requestParam.SlabDetails.Add(new SlabDetails() { MonthSequence = 2, Percentage = 26 });
            // requestParam.SlabDetails.Add(new SlabDetails() { MonthSequence = 3, Percentage = 12 });
            // requestParam.SlabDetails.Add(new SlabDetails() { MonthSequence = 4, Percentage = 12 });
            // requestParam.SlabDetails.Add(new SlabDetails() { MonthSequence = 5, Percentage = 12 });
            // requestParam.SlabDetails.Add(new SlabDetails() { MonthSequence = 6, Percentage = 12 });
            // requestParam.TermInMonth = 6;
            // requestParam.NumberOfDaysInMonth = 21;
            // requestParam.NumberOfDaysInWeek = 5;

            // //  requestParam.RemainingInterestAmount = 10;
            // // requestParam.PaymentStream = new List<PaymentStream>();
            // //var paymentStream = new PaymentStream();
            // //paymentStream.PaymentFrequency = PaymentFrequency.Monthly;
            // //paymentStream.NoOfPayments = 3;
            // //paymentStream.RepayablePercent = 50;
            // //requestParam.PaymentStream.Add(paymentStream);
            // //var paymentStream1 = new PaymentStream();
            // //paymentStream1.PaymentFrequency = PaymentFrequency.Weekly;
            // //paymentStream1.NoOfPayments = 3;
            // //paymentStream1.RepayablePercent = 50;

            // //requestParam.PaymentStream.Add(paymentStream1);

            // // var payback = new PaybackTier();
            // // payback.FinanceCharge = 1820;
            // // payback.Principal = 3333.33;
            // // payback.NoOfPayments = 2;
            // // payback.IsPercent = false;
            // // requestParam.PaybackTier = new List<PaybackTier>();
            // // requestParam.PaybackTier.Add(payback);
            // // var payback1 = new PaybackTier();
            // // payback1.FinanceCharge =1680;
            // // payback1.Principal = 6666.67;
            // // payback1.NoOfPayments = 4;
            // // payback1.IsPercent = false;
            // // requestParam.PaybackTier.Add(payback1);
            // //var payback2 = new PaybackTier();
            // //payback2.FinanceCharge = 420;
            // //payback2.Principal = 1666.67;
            // //payback2.NoOfPayments = 5;
            // //payback2.IsPercent = false;
            // //requestParam.PaybackTier.Add(payback2);
            // //var payback3 = new PaybackTier();
            // //payback3.FinanceCharge = 420;
            // //payback3.Principal = 1666.67;
            // //payback3.NoOfPayments = 4;
            // //payback3.IsPercent = false;
            // //requestParam.PaybackTier.Add(payback3);
            // //var payback4 = new PaybackTier();
            // //payback4.FinanceCharge = 420;
            // //payback4.Principal = 1666.67;
            // //payback4.NoOfPayments = 5;
            // //payback4.IsPercent = false;
            // //requestParam.PaybackTier.Add(payback4);
            // //var payback5 = new PaybackTier();
            // //payback5.FinanceCharge = 420;
            // //payback5.Principal = 1666.67;
            // //payback5.NoOfPayments = 5;
            // //payback5.IsPercent = false;
            // //requestParam.PaybackTier.Add(payback5);
            // var amortization = new LoanAmortization();
            // //var scheduleInput = new ScheduleInputDetails();
            // // scheduleInput.RoundingDigit = 2;
            // // scheduleInput.RoundingMethod = "bankers";
            // // var schdules = amortization.GetSCFActualSchedule(scheduleInput,true,20);
            // var schdules = amortization.GetAmortizationScedule(requestParam);



            var requestParam = new AmortizationRequest();
            requestParam.LoanAmount = 116000;
            requestParam.InterestRate = 20.3;
            //requestParam.FactorRate = 1.39;
            //requestParam.PaymentAmount = 100;
            requestParam.PaymentFrequency = PaymentFrequency.Daily;
            requestParam.Term = 13;
            requestParam.IsHolidaySchedule = true;
            requestParam.PortfolioType = "installment";
            requestParam.RoundingDigit = 2;
            requestParam.RoundingMethod = "mathematical";
            requestParam.FirstPaymentDate = new DateTime(2020, 02, 05);
            requestParam.OriginalFirstPaymentDate = new DateTime(2020, 02, 05);
            requestParam.FundedDate = new DateTime(2020, 01, 28);
            requestParam.ActualAmortizationType = "VariableDailyRate";
            requestParam.BillingDate = 5;
            requestParam.BrokenPeriodHandlingType = "separate";
            requestParam.BrokenPeriodInterestType = "Actual/Actual";
            requestParam.CarryForwardOutstanding = "Principal";
            requestParam.DailyAccrualMethod = "30/360";
            requestParam.IsDoubleScheduleForHoliday = false;
            requestParam.ScheduleType = ScheduleType.EquatedMonthlyInstalments;
            var amortization = new LoanAmortization();
            var schedules = amortization.GetAmortizationSchedule(requestParam);
            
        }
    }
}
