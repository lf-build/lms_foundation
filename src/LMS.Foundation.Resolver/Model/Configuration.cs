﻿namespace LMS.Foundation.Resolver
{
    public class Configuration : IConfiguration
    {
        public string Parameter { get; set; }
        public string Condition { get; set; }
        public string ConditionValue { get; set; }
        public string Event { get; set; }
        public string LambdaExpression { get; set; }
    }
}
