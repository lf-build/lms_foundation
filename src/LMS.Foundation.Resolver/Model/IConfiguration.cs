﻿namespace LMS.Foundation.Resolver
{
    public interface IConfiguration
    {
        string Condition { get; set; }
        string ConditionValue { get; set; }
        string LambdaExpression { get; set; }
        string Event { get; set; }
        string Parameter { get; set; }
    }
}