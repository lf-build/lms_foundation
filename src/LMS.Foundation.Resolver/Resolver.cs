﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace LMS.Foundation.Resolver
{
    public class Resolver
    {
        public Resolver()
        {
        }

        //public string ResolveCondition(string parameter, string resolveValue, List<IConfiguration> configurations)
        //{
        //    var eventName = string.Empty;
        //    var filteredConfigurations = configurations.Where(c => string.Equals(c.Parameter, parameter, StringComparison.InvariantCultureIgnoreCase));
        //    foreach (var configuration in filteredConfigurations)
        //    {
        //        eventName = Comparer(configuration, resolveValue);
        //        if (!string.IsNullOrWhiteSpace(eventName))
        //            break;
        //    }
        //    return eventName;
        //}

        //private string Comparer(IConfiguration configuration, string resolveValue)
        //{
        //    var eventName = string.Empty;
        //    switch (configuration.Condition.ToLower())
        //    {
        //        case "<":
        //            if (Convert.ToInt32(resolveValue) < Convert.ToInt32(configuration.ConditionValue))
        //                eventName = configuration.Event;
        //            break;
        //        case "<=":
        //            if (Convert.ToInt32(resolveValue) <= Convert.ToInt32(configuration.ConditionValue))
        //                eventName = configuration.Event;
        //            break;
        //        case ">":
        //            if (Convert.ToInt32(resolveValue) > Convert.ToInt32(configuration.ConditionValue))
        //                eventName = configuration.Event;
        //            break;
        //        case ">=":
        //            if (Convert.ToInt32(resolveValue) >= Convert.ToInt32(configuration.ConditionValue))
        //                eventName = configuration.Event;
        //            break;
        //        case "==":
        //            if (Convert.ToInt32(resolveValue) == Convert.ToInt32(configuration.ConditionValue))
        //                eventName = configuration.Event;
        //            break;
        //        case "!=":
        //            if (Convert.ToInt32(resolveValue) != Convert.ToInt32(configuration.ConditionValue))
        //                eventName = configuration.Event;
        //            break;
        //        case "equal":
        //            if (string.Equals(resolveValue, configuration.ConditionValue, StringComparison.InvariantCultureIgnoreCase))
        //                eventName = configuration.Event;
        //            break;
        //        case "notequal":
        //            if (!string.Equals(resolveValue, configuration.ConditionValue, StringComparison.InvariantCultureIgnoreCase))
        //                eventName = configuration.Event;
        //            break;
        //        case "greaterthan":
        //            if (DateTime.Compare(Convert.ToDateTime(resolveValue), Convert.ToDateTime(configuration.ConditionValue)) > 0)
        //                eventName = configuration.Event;
        //            break;
        //        case "lessthan":
        //            if (DateTime.Compare(Convert.ToDateTime(resolveValue), Convert.ToDateTime(configuration.ConditionValue)) < 0)
        //                eventName = configuration.Event;
        //            break;
        //        case "dateequal":
        //            if (DateTime.Compare(Convert.ToDateTime(resolveValue), Convert.ToDateTime(configuration.ConditionValue)) == 0)
        //                eventName = configuration.Event;
        //            break;
        //        default:
        //            break;
        //    }
        //    return eventName;
        //}

        public static string Parser<T>(string parameter, T resolveValue, List<IConfiguration> configurations)
        {
            ExpressionParser ep = new ExpressionParser();

            //LambdaExpression lambda = ep.Parse("(int x) => x < 30");
            var filteredConfigurations = configurations.Where(c => string.Equals(c.Parameter, parameter, StringComparison.InvariantCultureIgnoreCase));
            foreach (var configuration in filteredConfigurations)
            {
                LambdaExpression lambda = ep.Parse(configuration.LambdaExpression);
                var result = (bool)ep.Run(lambda, resolveValue);
                if (result)
                    return configuration.Event;
            }
            return string.Empty;
        }
    }
}
