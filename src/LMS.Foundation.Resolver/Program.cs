﻿using System.Collections.Generic;
using Newtonsoft.Json;
using System.Linq;
using System;

namespace LMS.Foundation.Resolver
{
    public static class Program
    {
        public static void Main(string[] args)
        {

            var json = "[{'Parameter':'DPD','LambdaExpression':'(int x) => x <= 30','Event':'E1'},{'Parameter':'DPD','LambdaExpression':'(int x) => x <= 60','Event':'E2'},{'Parameter':'DPD','LambdaExpression':'(int x) => x >= 90','Event':'E3'}]";
            var configurations = JsonConvert.DeserializeObject<List<Configuration>>(json);
            Console.WriteLine("Paramenter: DPD");
            Console.WriteLine("Value: " + 180);
            var result = Resolver.Parser("DPD", 180, configurations.ToList<IConfiguration>());
            Console.WriteLine("Event to be raised : " + result);
        }
    }
}
