using System;

namespace LMS.Foundation.Calculus
{
    public class Helper
    {
        public static int GetDaysInYear(int year)
        {
            var thisYear = new DateTime(year, 1, 1);
            var nextYear = new DateTime(year + 1, 1, 1);

            return (nextYear - thisYear).Days;
        }

        public static double GetDaysInYearForProvidedDate(DateTime processingDate, DateTime firstPaymentDate)
        {
            var endDate = firstPaymentDate.AddYears(1);
            if(processingDate.Date < endDate.Date)
            {
                return (endDate - firstPaymentDate).TotalDays;
            }
            else
            {
                return GetDaysInYearForProvidedDate(processingDate, endDate);
            }
        }
        public static DateTime? GetFirstPaymentDateBasedOnFrequency(string paymentFrequency, DateTime onBoardedDate)
        {
            DateTime? firstPaymentDateTime = null;
            switch (paymentFrequency.ToLower())
            {
                case "daily":
                    firstPaymentDateTime = onBoardedDate.AddDays(-1);
                    break;
                case "weekly":
                    firstPaymentDateTime = onBoardedDate.AddDays(-7);
                    break;
                case "biWeekly":
                    firstPaymentDateTime = onBoardedDate.AddDays(-14);
                    break;
                case "monthly":
                    firstPaymentDateTime = onBoardedDate.AddMonths(-1);
                    break;
                case "quarterly":
                    firstPaymentDateTime = onBoardedDate.AddMonths(-3);
                    break;
                case "yearly":
                    firstPaymentDateTime = onBoardedDate.AddYears(-1);
                    break;
                case "semiyearly":
                    firstPaymentDateTime = onBoardedDate.AddMonths(-6);
                    break;
                default:
                    break;
            }

            return firstPaymentDateTime;
        }

    }
}