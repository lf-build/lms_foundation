﻿using System;
using System.Linq;

namespace LMS.Foundation.Calculus
{
    public class IRR
    {
        private static double[] cashFlow;
        static int MIN_CASH_FLOW_INTERLOCK = 2;
        static double MAX_ITERATIONS = 10000;
        static double ACCEPTED_TOLERANCE = 0.000001;
        private static int iterationsNum;
        private static double result;
      
        private static bool IsValidCashFlows
        {

            get
            {
                if (cashFlow.Length < MIN_CASH_FLOW_INTERLOCK || (cashFlow[0] > 0))
                {
                    throw new ArgumentOutOfRangeException(
                        "Cash flow for the first period  must be negative");
                }
                return true;
            }
        }

        private static double InitialGuess
        {
            get
            {
                double initialGuess = cashFlow.Skip(1).Average() / cashFlow[0];
                return initialGuess;
            }
        }


        public static double Irr(double[] CashFlow)
        {
            cashFlow = CashFlow;
            if (IsValidCashFlows)
            {
                PerformNRCalc(InitialGuess);

                if (result > 1)
                    throw new Exception(
                        "Failed to calculate the IRR for the cash flow series. Please provide a valid cash flow sequence");
            }
            return result;
        }
        

        private static void PerformNRCalc(double estimatedReturn)
        {
            iterationsNum++;
            result = estimatedReturn - IRRPolynomialSum(estimatedReturn) / IRRDerivativeSum(estimatedReturn);
            while (!HasConverged(result) && MAX_ITERATIONS != iterationsNum)
            {
                PerformNRCalc(result);
            }
        }

        private static double IRRPolynomialSum(double estimatedReturnRate)
        {
            double sumOfPolynomial = 0;
            if (IsWithinBounds(estimatedReturnRate))
                for (int j = 0; j < cashFlow.Length; j++)
                {
                    sumOfPolynomial += cashFlow[j] / (Math.Pow((1 + estimatedReturnRate), j));
                }
            return sumOfPolynomial;
        }


        private static bool HasConverged(double estimatedReturnRate)
        {
            bool isWithinTolerance = Math.Abs(IRRPolynomialSum(estimatedReturnRate)) <= ACCEPTED_TOLERANCE;
            return (isWithinTolerance) ? true : false;
        }


        private static double IRRDerivativeSum(double estimatedReturnRate)
        {
            double sumOfDerivative = 0;
            if (IsWithinBounds(estimatedReturnRate))
                for (int i = 1; i < cashFlow.Length; i++)
                {
                    sumOfDerivative += cashFlow[i] * (i) / Math.Pow((1 + estimatedReturnRate), i);
                }
            return sumOfDerivative * -1;
        }


        private static bool IsWithinBounds(double estimatedReturnRate)
        {
            return estimatedReturnRate != -1 && (estimatedReturnRate < int.MaxValue) &&
                   (estimatedReturnRate > int.MinValue);
        }

    }
}
