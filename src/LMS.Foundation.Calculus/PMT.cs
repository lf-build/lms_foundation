﻿using System;


namespace LMS.Foundation.Calculus
{
    public class PMT
    {
        public static double Calculate_PMT(double loanAmount, double term, double rate)
        {
            double AnnualInterest = rate / 100;
            double EmiAmount = loanAmount * AnnualInterest * (Math.Pow((1 + AnnualInterest), term)) / ((Math.Pow((1 + AnnualInterest), term) - 1));
            return EmiAmount;
        }

    }
}
