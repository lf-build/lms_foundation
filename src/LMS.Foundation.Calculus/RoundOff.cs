﻿using System;

namespace LMS.Foundation.Calculus
{
    public class RoundOff
    {
        public static double Round(double value, int digits, string roundingType)
        {
            switch (roundingType.ToLower())
            {
                case "mathematical":
                    return Math.Round(value, digits, MidpointRounding.AwayFromZero);
                case "bankers":
                    return Math.Round(value, digits);
                default:
                    return value;
            }
        }

        public static double Truncate(double value, int digits)
        {
            double stepper = (double)(Math.Pow(10.0, (double)digits));
            int temp = (int)(stepper * value);
            return (double)temp / stepper;
        }


    }
}
