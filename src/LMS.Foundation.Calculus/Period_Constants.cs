﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Foundation.Calculus
{
    public static class Period_Constants
    {
        public static int Days360  = 360;
        public static int Days365  = 365;
        public static int Days364  = 364;
        public static int Weekly_Period_per_year = 52;
        public static int BiWeekly_Period_per_year = 26;
        public static int Monthly_Period_per_year= 12;
        public static int Quarterly_Period_per_year = 4;
        public static int Semi_Yearly_Period_per_year = 6;
        public static int Weekly_Period_per_days = 7;
        public static int BiWeekly_Period_per_days = 14;
        public static int Monthly_Period_per_days = 30;
        public static int Quarterly_Period_per_days = 90;
        public static int Semi_Yearly_Period_per_days = 180;

    }

}
