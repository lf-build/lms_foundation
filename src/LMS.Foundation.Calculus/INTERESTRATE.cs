﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Foundation.Calculus
{
    public class InterestRate
    {
        public static double Calculate_InterestRate(string PaymentFrequency, double annualRate, int periodConstant = 360)
        {
            double compound_period = 0;
            switch (PaymentFrequency.ToString().ToLower())
            {
                case "daily":
                    compound_period = periodConstant;
                    break;
                case "weekly":
                    compound_period = Period_Constants.Weekly_Period_per_year;
                    break;
                case "biweekly":
                    compound_period = Period_Constants.BiWeekly_Period_per_year;
                    break;
                case "monthly":
                    compound_period = Period_Constants.Monthly_Period_per_year;
                    break;
                case "quarterly":
                    compound_period = Period_Constants.Quarterly_Period_per_year;
                    break;
                case "semiyearly":
                    compound_period = Period_Constants.Semi_Yearly_Period_per_year;
                    break;
                case "yearly":
                    compound_period = annualRate;
                    break;
                default:
                    throw new InvalidOperationException("Invalid Payment Frequency");                    
            }

            return (Math.Pow((1 + annualRate/ compound_period), compound_period/ compound_period) - 1) ;
        }

        public static double InterestRate_MCA( double principalOutstanding ,double interestRate)
        {
            return (interestRate / principalOutstanding) * 100;
        }

    }
}
